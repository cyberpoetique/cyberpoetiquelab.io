// Scripts
// Menu
const menuBtn = document.querySelector('.btn--menu');
const menuPage = document.querySelector('.menu');
let menuShow = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuPage.classList.toggle('show--menu');
  document.querySelector('.icone--info').classList.toggle('rotation');
  menuShow = !menuShow;
});

const menuStop = function(e) {
  if (menuShow == true) {
    const clickInsideMenu = menuPage.contains(e.target);
    const clickInsideBtn = menuBtn.contains(e.target);
    if (!clickInsideMenu && !clickInsideBtn) {
      menuPage.classList.toggle('show--menu');
      document.querySelector('.icone--info').classList.toggle('rotation');
      menuShow = !menuShow;
    }
  }
}
document.addEventListener('click', menuStop);
document.addEventListener('touchstart', menuStop);

// For Baffle - https://github.com/camwiegert/baffle
// With options
let b = baffle('.baffle')
        .reveal(500)
        .set({
            characters: 'cyberpoétique',
            speed: 50
});

// For Lazyload - https://github.com/verlok/lazyload/
// var callback_enter = function(el) {
//     el.parentNode.classList.add('image--loading');
//   };
//   var callback_loaded = function(el) {
//     el.parentNode.classList.remove('image--loading');
//   };
var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    thresholds: "500px 10%",
    // callback_enter: callback_enter,
		// callback_loaded: callback_loaded
});
// After your content has changed...
// myLazyLoad.update();

// FancyBox
$('[data-fancybox="images"]').fancybox({
  buttons : [
    'zoom',
    'fullScreen',
    'thumbs',
    'close'
  ],
  infobar: false,
  arrows: true,
  loop: true,
   lang: "fr",
    i18n: {
        en: {
            CLOSE: "Close",
            NEXT: "Next",
            PREV: "Previous",
            ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
            PLAY_START: "Start slideshow",
            PLAY_STOP: "Pause slideshow",
            FULL_SCREEN: "Full screen",
            THUMBS: "Thumbnails",
            DOWNLOAD: "Download",
            SHARE: "Share",
            ZOOM: "Zoom"
        },
        fr: {
            CLOSE: "Fermer",
            NEXT: "Suivant",
            PREV: "Précédent",
            ERROR: "Le contenu demandé ne peut pas être affiché. <br/> Merci d'essayer plus tard.",
            PLAY_START: "Débuter le diaporama",
            PLAY_STOP: "Arrêter le diaporama",
            FULL_SCREEN: "Plein écran",
            THUMBS: "Galerie",
            DOWNLOAD: "Herunterladen",
            SHARE: "Partager",
            ZOOM: "Zoomer"
        }
    }
});


// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.menu');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});
