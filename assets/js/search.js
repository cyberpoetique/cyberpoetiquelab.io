// Search Engine with Lunrjs
// This code from Hugo Lithium Theme - Janik von Rotz - MIT License
// https://github.com/janikvonrotz/hugo-lithium-theme/blob/master/exampleSite/content/page/search.md
// https://janikvonrotz.ch/2019/06/10/2019-06-10-simple-hugo-page-search-with-lunrjs/
//
// define globale variables
var idx, searchInput, searchResults = null
var documents = []

function renderSearchResults(results){

    if (results.length > 0) {

        // show max 10 results
        if (results.length > 9){
            results = results.slice(0,10)
        }

        // reset search results
        searchResults.innerHTML = ''

        // append results
        results.forEach(result => {

            // create result item
            var article = document.createElement('article')
            article.innerHTML = `
            <h2 class="search__title"><a href="${result.ref}">${documents[result.ref].title}</a></h2>
            <section class="search__summary">
            <p class="search__summary">${documents[result.ref].summary}</p>&nbsp;<span class="read-more"><a href="${result.ref}">[&hellip;]</a></span>
            `
            searchResults.appendChild(article)
        })

    // if results are empty
    } else {
        searchResults.innerHTML = '<p>&hellip;le vide qui n’existe pas.</p>'
    }
}

function registerSearchHandler() {

    // register on input event
    searchInput.oninput = function(event) {

        // remove search results if the user empties the search input field
        if (searchInput.value == '') {

            searchResults.innerHTML = ''
        } else {

            // get input value
            var query = event.target.value

            // run fuzzy search
            var results = idx.search(query + '*')

            // render results
            renderSearchResults(results)
        }
    }

    // set focus on search input and remove loading placeholder
    // searchInput.focus()
    // searchInput.placeholder = ''
}

window.onload = function() {

    // get dom elements
    searchInput = document.getElementById('search-input')
    searchResults = document.getElementById('search-results')

    // request and index documents
  fetch('/index.json', {
        method: 'get'
    }).then(
        res => res.json()
    ).then(
        res => {

            // index document
            idx = lunr(function() {
                // issue with wildcard
                // https://github.com/olivernn/lunr.js/issues/454
                this.pipeline.remove(lunr.stemmer);
                this.pipeline.remove(lunr.stopWordFilter);
                this.ref('url')
                this.field('title')
                this.field('content')
                this.field('summary')
                this.field('date')
                this.field('tags')
                this.field('categories')
                this.field('authors')

                res.forEach(function(doc) {
                    this.add(doc)
                    documents[doc.url] = {
                        'title': doc.title,
                        'content': doc.content,
                        'summary': doc.summary,
                        'date': doc.date,
                        'tags': doc.tags,
                        'categories': doc.categories,
                        'authors': doc.authors,
                    }
                }, this)
            })

            // data is loaded, next register handler
            registerSearchHandler()
        }
    ).catch(
        err => {
            searchResults.innerHTML = `<p>${err}</p>`
        }
    )
}

