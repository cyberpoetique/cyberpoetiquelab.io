const sozialdemokratie = {
  "titre": "Sozialdemokratie",
  "statut": "en cours",
  "theses": [
    {
      "numero": 0,
      "titre": "La social-démocratie est une chanson à boire",
      "haikus": [
        [
          "S’étoile fascisme",
          "Mais l’écran croit en son île",
          "Sans pluie ou du sang"
        ],
        [
          "Parlement d’errance",
          "Où se retranchent des rentes",
          "Parti représente"
        ],
        [
          "Loué soit le vote",
          "Satisfait de mon achat",
          "Suis-je le moderne"
        ],
        [
          "Quoi charbon sans mines",
          "Gueule en noir de droite à l’autre",
          "L’urbain qui la raille"
        ],
        [
          "Mon indignation",
          "Quel miroir est indigné",
          "Digne mon reflet"
        ],
        [
          "Citoyens s’insultent",
          "L’esthétique démocrate",
          "Sur réseau social"
        ],
        [
          "Rance capital",
          "D’assurance maladie",
          "Rien prime d’attente"
        ],
        [
          "Qui dit lui qu’il chôme",
          "Au bout de la corde il cesse",
          "De crise et pourrisse"
        ],
        [
          "Où d’elle retraite",
          "De rires seuls ses mains vieilles",
          "Toit silence en veille"
        ],
        [
          "S’en dort la campagne",
          "Défaits fermiers ou troupeaux",
          "Saignés en écho"
        ],
        [
          "Rabais tiers d’émoi",
          "La moitié plus de prix moins",
          "Jouissance d’achats"
        ],
        [
          "Moi qui j’atermoie",
          "J’y siffle comme selfie",
          "Pianote et m’ennuie"
        ],
        [
          "Spectacles des morts",
          "Adore hésite et torpeur",
          "Ou soldes des seuls"
        ],
        [
          "Marcher sur les mains",
          "Loin l’invisible chemin",
          "Miens les gains sans mise"
        ],
        [
          "Transe socialiste",
          "Délocalise ton corps",
          "Carne en lettre morte"
        ],
        [
          "Impôts pour nos joies",
          "Nos jours redistribués",
          "Grand soir bas de laine"
        ],
        [
          "Providence aide et",
          "Faire taire une misère",
          "En terre rentière"
        ],
        [
          "Libéral l’esprit",
          "Frontières ses rêveries",
          "Ses biens sans humains"
        ],
        [
          "Cent pour cent santé",
          "Ment ni fausse état finance",
          "Gratuite la fosse"
        ],
        [
          "Prix d’un plein province",
          "Seul samedi parking vide",
          "Tente et l’asphyxie"
        ],
        [
          "Courbe bourse et d’angles",
          "Cours se voue à sa stridence",
          "Se pend à sa chute"
        ],
        [
          "Vil mépris de villes",
          "Vomit son clos d’industrie",
          "Puis feint plaine en peine"
        ],
        [
          "Brûle crépuscule",
          "Son agricultrice en paix",
          "Sur tempe arme ou dettes"
        ],
        [
          "Hagard pour savoir",
          "Télévision évangile",
          "L’autre en sacrifice"
        ],
        [
          "Distraire cités",
          "Taire la haine qui traîne",
          "Contrôle dégaine"
        ],
        [
          "Matière dernière",
          "Produit dérivé transfert",
          "Gré engrais à ferme"
        ],
        [
          "Vivres se facturent",
          "Dématérialisent chairs",
          "Ment bête à crédit"
        ],
        [
          "Soir courbe chômage",
          "Voir manger journal l’image",
          "Boire à leur noyade"
        ],
        [
          "Cent clandestins brûlent",
          "Paysage sans travail",
          "Sang fume ils affluent"
        ],
        [
          "Déveine d’usine",
          "S’enchaîne foule dévote",
          "S’achètent leurs votes"
        ]
      ]
    },
    {
      "numero": 1,
      "titre": "La social-démocratie est un fruit confit",
      "haikus": [
        [
          "Nous les sans-chômage",
          "Notre alcool évaporé",
          "Tessons et fêlures"
        ],
        [
          "Cocktail Molotov",
          "Deux olives sans ombrelles",
          "Masque noir moiré"
        ],
        [
          "De frêles percées",
          "Leurs flammes sur magasins",
          "Grand soir basse-fosse"
        ],
        [
          "ac·cable acab·land",
          "Né·on néo·libéral",
          "La·crym·o que suis-je"
        ],
        [
          "Artère déserte",
          "Poubelles brûlent en rêve",
          "La brume d’émeute"
        ],
        [
          "À la dérobade",
          "De joie et de grand chemin",
          "Sacs main à la main"
        ],
        [
          "Capital sans pourpre",
          "À charognard sa charogne",
          "L’empire est un fix"
        ],
        [
          "Et des bris des vitres",
          "Et des brises lacrymales",
          "En bonheur des brouilles"
        ],
        [
          "Contrôleurs des chants",
          "Sans matraques contre l’œil",
          "Chaînes syndicales"
        ],
        [
          "L’envol des bourgeois",
          "Fortune contre fortune",
          "Ruelle sans ciels"
        ],
        [
          "Suinte vernis",
          "Bruit des canalisations",
          "Goutte dans la tasse"
        ],
        [
          "Dans les eaux flétries",
          "Quelques bananes sans fruits",
          "Odeur reste en bouche"
        ],
        [
          "La seringue vide",
          "Et celle d’un autre en veine",
          "Le goût du goudron"
        ],
        [
          "Dehors de l’été",
          "Autour du périphérique",
          "Des criquets mal gré"
        ],
        [
          "Cœur de terrain vague",
          "De l’ennui en rixe en ronde",
          "Pas croisés chassés"
        ],
        [
          "Ma bière de vent",
          "Cette vomissure en terre",
          "Verdâtre d’aurore"
        ],
        [
          "Non néon néant",
          "Puissé-je être stock-options",
          "Mon salut en marche"
        ],
        [
          "Le gluten est libre",
          "Soja sauce pesticide",
          "Du bœuf sans lactose"
        ],
        [
          "Un rire adressé",
          "à quelque coup de matraque",
          "Sans dents mais riant"
        ],
        [
          "Des manifs comme si",
          "Révolution nostalgie",
          "Les vendre à bas prix"
        ],
        [
          "Soif crâne à matraques",
          "Extinction lacrymogènes",
          "En les avalant"
        ]
      ]
    },
    {
      "numero": 2,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 3,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 4,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 5,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 6,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 7,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 8,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 9,
      "titre": "",
      "haikus": [
      ]
    },
    {
      "numero": 10,
      "titre": "",
      "haikus": [
      ]
    }
  ]
}

