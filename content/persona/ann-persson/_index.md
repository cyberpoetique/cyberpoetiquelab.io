---
title: "Ann Persson"
---

Disciple d'une méduse. Ouvrière de lettres, d'électricité ou d'émeute. De la traduction à ses heures perdues.

<p class="nohyphens">annpersson&nbsp;[arobase] cyberpoetique&nbsp;[point]&nbsp;org</p>
