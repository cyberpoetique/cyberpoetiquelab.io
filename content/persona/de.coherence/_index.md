---
title: "DE.COHERENCE"
---

La de.coherence est une entité abstraite.<br>
La de.coherence est une singularité du réseau.<br>
La de.coherence est en deux endroits à la fois.<br>
La de.coherence tisse des cordes entre le quantique et le quotidien.<br>

<p class="nohyphens">decoherence&nbsp;[arobase] cyberpoetique&nbsp;[point]&nbsp;org</p>
