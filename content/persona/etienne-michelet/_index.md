---
title: "Étienne Michelet"
---

Né en 1984. Vit actuellement en France.
Écrit un journal, avec animaux, lune, fleurs et des fantômes aussi.
Enregistre sa voix, parfois, pour y voir mieux.

Son site : [DONGMURI](https://www.dongmuri.net/).
