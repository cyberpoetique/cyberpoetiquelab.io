---
title: "Otto Borg"
---

Post-punk, post-ouvrier, punk quand même, ouvrier encore. Otto Borg est né en Allemagne. Quelque part à l’ombre du mur, sous les nuées de Tchernobyl. Le reste est commun à son siècle. L’usine, tourneur sur métaux. Puis le chômage. Puis une reconversion. Puis menuisier. Un peu musicien aussi. Écrivain, toujours, pour survivre dans l’usine et dans le siècle.
