---
title: "Anthropie"
---

[anthropie](http://anthropie.art) est un collectif d’écriture audiovisuelle pour enfants (sauvages)

[anthropie](http://anthropie.art) est un geste en constante augmentation

[anthropie](http://anthropie.art) est une cellule esthético-procrastino-insurrectionnelle

[anthropie](http://anthropie.art) prône le times new roman, l’anonymat, l’open-source, le DIY, le hack des machines et des esprits
