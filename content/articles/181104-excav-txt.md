---
title: "excav.txt"
mot: "excav"
date: "2018-11-04"
description: "La piraterie littéraire n’est jamais finie. Le clavier entre les dents, nos mains animales frappent la matière, elles martèlent le réseau dans l’espoir d’entendre un quelconque écho, et notre quête digitale s’efforce de dévoiler le langage obscur de notre futur. Construire le commun de nos aventures débute par la destruction de notre grammaire."
images:
  - "img/181104-excavtxt.jpg"
sourceimage: "H. Af Klint, The Swan, N. 18 / https://commons.wikimedia.org/wiki/File:Hilma_Af_Klint_-_1915_-_The_Swan,_No._18.jpg"
persona:
  - "Le Réseaü"
espace:
  - "codex.cpp"
echo:
  - "graphique"
  - "glitch"
  - "microantilivre"
  - "transdialectique"
---

«&nbsp;La piraterie littéraire n’est jamais finie. Le clavier entre les dents, nos
mains animales frappent la matière, elles martèlent le réseau dans l’espoir
d’entendre un quelconque écho, et notre quête digitale s’efforce de dévoiler le
langage obscur de notre futur. Construire le commun de nos aventures débute par
la destruction de notre grammaire. [&hellip;]&nbsp;»

<h1 id="excav" class="glitch--continu" data-text="∑Xℂ∀V.ϮΧϮ">∑Xℂ∀V.ϮΧϮ</h1>

«&nbsp;[&hellip;] Notre rêve commande d’abattre les murs, de ne jamais les remplacer par des frontières nouvelles, d’étendre les horizons jusqu’à ce que l’œil se perde loin de nos mondes. Et au cœur de cette tâche sorcière qui augure les cycles à venir de nos communications, l’outil des renaissances ne peut être autre que le lien qui nous rive à notre présent : la littérature ou cette cloison de nos échanges humains. [&hellip;]&nbsp;»

<h2 id="antilivre" class="glitch--continu" data-text="ªℕ†ïLι√я∉">ªℕ†ïLι√я∉</h2>

*L’hybridation du chiffre et de la lettre <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_excavtxt.pdf">avec l’antilivre</a>, dans son format clandestin de [microantilivre](https://abrupt.cc/antilivre#microantilivre), cette version imprimable, pliable, brûlable <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_excavtxt_imprimable.pdf">à la façon DIY,</a> puisque les punks ne meurent jamais.*

{{< figurelazy src="/img/181104-voyager-rouge.gif" alt="Glitch du Disque Voyager Golden Record de la sonde Voyager" width="100%" >}}

«&nbsp;[&hellip;] L’humain, le moderne s’est isolé derrière une invisible limite, le nommable.
C’est dans cette faille que le marteau numérique doit frapper, en désignant
*l’innommable*, en *excavant* l’information brute qui se terre sous le langage. [&hellip;]&nbsp;»

<h2 id="html" class="glitch--continu ligneblanchehaut" data-text="ĦŦṂŁ">ĦŦṂŁ</h2>

*Et voilà que le langage offre au langage, que le JavaScript accorde à la littérature <a href="https://abrupt.cc/lab/excav.txt/">une dynamique</a> de sa texture, une transcendance de ses hésitations.*

«&nbsp;[&hellip;] C’est cette substance qui doit être sondée, et par la fission des graphies, une
*excavation* de la moelle informationnelle dissimulée dans le texte fait place
au devenir de notre appréhension humaine du réel. Il n’y a pas d’objectivité
par le langage, mais un agglomérat d’interprétations. Éroder l’illusion
graphique de nos littératures qui se veulent objectives aide à se placer au
plus près de l’essence mouvante de l’information, et d’y inscrire nos
interprétations en parallèle de cette dynamique naturelle. [&hellip;]&nbsp;»

<video autoplay loop muted playsinline>
  <source src="/img/181104-negatif.mp4" type="video/mp4">
</video>

<h2 id="audio" class="glitch--continu ligneblanchehaut" data-text="4üĐ!Ω">4üĐ!Ω</h2>

*La parole se divise, mais elle ne peut trahir davantage que l’écriture qui grave ses imperfections en affirmant l’intemporalité de sa raison.*

{{< soundcloud 524701200 >}}

«&nbsp;[&hellip;] Que faire ? Chercher à détisser le texte, à abolir le principe même de surface
qui recueillerait nos graphies. La graphie est déjà en soi une surface, celle
de l’information, qui peut aujourd’hui être aisément saisissable dans sa
traduction en binaire. [&hellip;]&nbsp;»

<h2 id="video" class="glitch--continu" data-text="∇1D3Ø">∇1D3Ø</h2>

*L’image trace l’esquisse d’une ère nouvelle, lorsqu’elle dépasse la limite de son écoulement.*

{{< youtube 6DFgn3yDA7k >}}

<p class="ligneblanchehaut">«&nbsp;[&hellip;] La littérature doit se débarrasser de ses contraintes, et quel meilleur moyen
d’entamer cet affranchissement que celui de détruire le livre. Cette
destruction n’est pas à comprendre comme un autodafé, un <span class="italique">acte de foi, mais
précisément comme l’anéantissement de la foi, celle en le langage. Se révèle
ainsi ce qui s’y dissimule, les ombres de l’information, l’immatériel brut, le
<span class="italique">chiffre. Le livre en lui-même porte une histoire de la barrière. [&hellip;]&nbsp;»</p>

<h2 id="brut" class="glitch--continu" data-text="tєϰтε ßℝЦΤ">tєϰтε ßℝЦΤ</h2>

*Les signes simples du calcul avec <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_excavtxt_texte_brut.txt">un texte brut</a> qui se rapproche de l’essence informationnelle du numérique.*

«&nbsp;[&hellip;] Le combat s’annonce, et comme nous l’a appris l’histoire, la
littérature est l’étincelle qui embrase la société, alors dans cette lutte où
les propriétaires dominent, où la liberté semble si précaire, il faut faire feu
littéraire, offrir aux multiples dimensions du texte leur indépendance, pirater
les normes numériques pour instaurer une idée : l’autonomie créatrice.&nbsp;»

{{< figurehover src1="/img/181104-ascii.jpg" src2="/img/181104-ascii-2.jpg" alt="ASCII art" width="100%" >}}

*De l’image cunéiforme aux rêves intersidéraux, le texte est abrupt.*

*(Toutes les dimensions de ce texte &mdash; versions audio, vidéo, dynamique, brute, antilivresque &mdash; se trouvent dans le domaine public volontaire sous la licence Creative Commons Zero, comme le précise notre politique de [partage](https://abrupt.cc/partage).</a>)*

{{< figurelazy src="/img/181104-cuneiforme.jpg" alt="Excav.txt cunéiforme" width="100%" >}}

*(Le Réseaü @ <a href="https://edhea.ch/" target="_blank">EDHEA</a>)*
