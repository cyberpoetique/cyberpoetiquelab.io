---
title: "La social&#8209;démocratie est un fruit confit"
slug: "haiku"
mot: "haïku"
date: "2020-05-08"
description: "Tu as le choix. D’errer parmi les haïkus d’origine. Ou de ne rien respecter. De mélanger les vers. De t’y terrer. Et de t’en faire un mix pour fix. Tu peux même remonter à la source. Parcourir l’histoire électrique du verbe."
images:
  - "img/200508-sozialdemokratie.jpg"
sourceimage: "James Ensor, L'entrée du Christ dans Bruxelles / https://commons.wikimedia.org/wiki/File:Christ%27s_Entry_into_Brussels_in_1889.jpg"
persona:
  - "Otto Borg"
espace:
  - "codex.cpp"
echo:
  - "Borg"
  - "haïku"
  - "social-démocratie"
  - "situations"
library:
  - "baffle.min.js"
  - "borg-otto-sozialdemokratie.js"
script: |
  let haikusListe = [];
  let haikusVersCinq = [];
  let haikusVersSept = [];
  const versCinqUn = document.querySelector('.haiku--original .vers--cinq-un');
  const versSept = document.querySelector('.haiku--original .vers--sept');
  const versCinqDeux = document.querySelector('.haiku--original .vers--cinq-deux');
  const mixVersCinqUn = document.querySelector('.haiku--mix .vers--cinq-un');
  const mixVersSept = document.querySelector('.haiku--mix .vers--sept');
  const mixVersCinqDeux = document.querySelector('.haiku--mix .vers--cinq-deux');
  const nombre = document.querySelector('.nombre');
  const theses = document.querySelector('.theses');
  const btnHaiku = document.querySelector('.btn--haiku');
  const btnHaikuMix = document.querySelector('.btn--haiku-mix');
  let nbTitres = 0;

  for (let i = 0; i < sozialdemokratie.theses.length ; i++) {
    for (let j = 0; j < sozialdemokratie.theses[i].haikus.length; j++) {
      haikusListe.push(sozialdemokratie.theses[i].haikus[j]);
      haikusVersCinq.push(sozialdemokratie.theses[i].haikus[j][0]);
      haikusVersSept.push(sozialdemokratie.theses[i].haikus[j][1]);
      haikusVersCinq.push(sozialdemokratie.theses[i].haikus[j][2]);
    }
    nombre.innerHTML = haikusListe.length + ' ';
    if (sozialdemokratie.theses[i].titre != "") {
      nbTitres++;
      let li = document.createElement('li');
      li.innerHTML += sozialdemokratie.theses[i].numero + '. ';
      li.innerHTML += sozialdemokratie.theses[i].titre;
      theses.appendChild(li);
    }
  }
  if (nbTitres < 11) {
    let last = document.createElement('li');
    last.innerHTML = '(À suivre...)';
    theses.appendChild(last);
  }

  function shuffle(array) {
      for (let i = array.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
      }
  }
  
  function showHaiku() {
    shuffle(haikusListe);
    shuffle(haikusListe);
    versCinqUn.innerHTML = haikusListe[0][0];
    versSept.innerHTML = haikusListe[0][1];
    versCinqDeux.innerHTML = haikusListe[0][2];

    let versHaiku = baffle('.haiku--original .vers')
      .reveal(500)
      .set({
        characters: 'sozialdemokratie',
        speed: 50
      });
  }
  
  function showHaikuMix() {
    shuffle(haikusVersCinq);
    shuffle(haikusVersSept);
    mixVersCinqUn.innerHTML = haikusVersCinq[0];
    mixVersSept.innerHTML = haikusVersSept[0];
    mixVersCinqDeux.innerHTML = haikusVersCinq[1];

    let versHaikuMix = baffle('.haiku--mix .vers')
      .reveal(500)
      .set({
        characters: 'sozialdemokratie',
        speed: 50
      });
  }

  showHaiku();
  showHaikuMix();

  btnHaiku.addEventListener('click', (e) => {
    showHaiku();
  });

  btnHaikuMix.addEventListener('click', (e) => {
    showHaikuMix();
  });
  
---

<div class="haiku haiku--original centre">
<p class="noindent vers vers--cinq-un pasdeligneblanche"></p>
<p class="noindent vers vers--sept pasdeligneblanche"></p>
<p class="noindent vers vers--cinq-deux pasdeligneblanche"></p>
</div>

<button class="btn centre btn--center btn--submit btn--haiku shake" type="submit" id="submit">au hasard, l’authentique</button>

<div class="haiku haiku--mix centre">
<p class="noindent vers vers--cinq-un pasdeligneblanche"></p>
<p class="noindent vers vers--sept pasdeligneblanche"></p>
<p class="noindent vers vers--cinq-deux pasdeligneblanche"></p>
</div>

<button class="btn btn--center btn--submit btn--haiku-mix shake" type="submit" id="submit">au hasard, l’irrévérence</button>

<br>
<br>
<br>

<div class="ligneblanche italique centre">
Tu as le choix.<br>D’errer parmi les haïkus d’origine.<br>Ou de ne rien respecter.<br>De mélanger les vers. De t’y terrer.<br>Et de t’en faire un mix pour fix. 
</div>

<div class="ligneblanche italique centre">
Tu peux même remonter à la source.<br>Parcourir l’histoire électrique du <a href="https://gitlab.com/cyberpoetique/cyberpoetique.gitlab.io/-/blob/master/static/js/borg-otto-sozialdemokratie.js" alt="la social-démocratie est une chanson à boire">verbe.</a><br>
</div>

<div class="ligneblanche italique centre">
Ces <span class="nombre"></span>textes font partie d’un ensemble de 999 haïkus.
</div>

<div class="ligneblanche italique centre">
Le grand œuvre a débuté <a href="https://abrupt.cc/otto-borg/la-social-democratie-est-une-chanson-a-boire/" alt="la social-démocratie est une chanson à boire">ici.</a><br>
Il comprendra 11 thèses, dont les titres sont :
<ul class="theses" style="list-style: none;"></ul>
</div>
