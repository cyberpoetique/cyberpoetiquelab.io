---
title: "Quarante-trois points pour un perspectivisme anarchitectural"
mot: "perspectives"
date: "2019-05-09"
description: "La lumière devient la lumière pour l’être qui ne s’y limite pas. [...] L’œil obscur devient la lumière qui anarchitecture la cité par sa pratique dialectique."
images:
  - "img/190509-perspectivisme-anarchitectural.jpg"
sourceimage: "Rozanova, Suprematism / https://commons.wikimedia.org/wiki/File:Suprematism_(Rozanova,_1916).jpg"
persona:
  - "La Colonne Horizon"
espace:
  - "fabrica.f"
echo:
  - "placard"
  - "anarchitecture"
  - "praxis"
---

*C’est avec l’idée d’anarchitecture et la pensée perspectiviste du groupe* La Colonne Horizon *que nous introduisons une nouvelle déclinaison de nos antilivres, de cette volonté que le texte tempête, se partage, forge son devenir graphique, se placarde pour clamer sa puissance cybernétique&nbsp;:* [les placards abrupts.](https://www.antilivre.org/#placard)

Ce placard est disponible dans son format original A3 pour être <a href="https://txt.abrupt.cc/antilivre/placard_abrupt_la_colonne_horizon_quarante-trois_points_pour_un_perspectivisme_anarchitectural.pdf">lu, imprimé, placardé.</a>

<h1 class="glitch" data-text="Quarante-trois points pour un perspectivisme anarchitectural">Quarante-trois points pour un perspectivisme anarchitectural</h1>

<h2 class="glitch ligneblanche" data-text="par La Colonne Horizon">par La Colonne Horizon</h2>

<ol class="enumeration" start="0">
<li>La lumière devient la lumière pour l’être qui ne s’y limite pas.</li>

<li>La transparence construit une idéologie des surfaces.</li>

<li>La transparence représente la vérité qui empêche la multiplication des visions de l’être.</li>

<li>La transparence se pare du mensonge de la clarté, masque les ombres révélatrices.</li>

<li>La transparence joue de reflets pour se jouer du réel.</li>

<li>La transparence ne révèle point de perspectives pour l’être qui se situe à la suite de l’horizon.</li>

<li>La transparence cloisonne l’être, parce qu’elle sépare de ce qu’elle prétend révéler.</li>

<li>La transparence doit être combattue avec la clairvoyance de l’œil qui entend les ombres.</li>

<li>La fenêtre manifeste sa clarté en l’image de la fenêtre.</li>

<li>La fenêtre obstrue le réel en le hiérarchisant, puisque toute hiérarchie demeure déformation animale.</li>

<li>La fenêtre cache la matière en la trahissant.</li>

<li>La fenêtre n’a pas d’ombre sincère.</li>

<li>La fenêtre s’étire pour étirer son autorité.</li>

<li>La fenêtre exprime la vulgarité d’une liberté planifiée face à la fatalité de la poterne.</li>

<li>La fenêtre est un royaume de la transparence qui appelle à la révolution des ombres.</li>

<li>Le seuil n’a pas de finalité, il est un monde mouvant qui se déploie entre les représentations.</li>

<li>Le seuil est en soi la traversée dans l’œil qui le contemple.</li>

<li>Le seuil nie la négation d’une géométrie qui dispose.</li>

<li>Le seuil développe l’imaginaire en deçà de l’ordinaire.</li>

<li>Le seuil a l’introspection des marges.</li>

<li>Le seuil propage les perspectives pour l’être qui se métamorphose en seuil.</li>

<li>Le seuil forge des dimensions nouvelles dans l’abandon de l’unité.</li>

<li>Le mur se répand en divisions de l’univers.</li>

<li>Le mur laisse davantage d’autonomie à la lumière que le verre.</li>

<li>Le mur glorifie en son apparence la matière qui cache.</li>

<li>Le mur conte, tandis que la vitre dicte.</li>

<li>Le mur compose la cité en disposant l’obstacle à surmonter.</li>

<li>Le mur interprète l’absence de ce qui prime en guidant la marche céleste de l’espace.</li>

<li>Le mur ordonne l’horizon.</li>

<li>L’obscur s’offre à qui voit par-delà les murs.</li>

<li>L’obscur détruit le divertissement pour instituer la tragédie en langage.</li>

<li>L’obscur se place dans le renversement de la géométrie.</li>

<li>L’obscur sculpte l’indestructible instant qui affirme la volonté de bâtir le temps en tant qu’espace.</li>

<li>L’obscur se montre à la lumière du cachot, chante la ruine.</li>

<li>L’obscur structure la dispersion de ce qui progresse, s’efface déjà.</li>

<li>L’obscur griffe l’œil qui se retourne en lui-même, en extension de son espace.</li>

<li>L’œil se trouve dans le regard des sables.</li>

<li>L’œil qui s’est initié à se taire peut édifier le foyer.</li>

<li>L’œil évide le temps qui dénie sa ruine, il délie la flamme de la pierre.</li>

<li>L’œil dresse le mur, abat le mur, en fait le seuil d’une lumière qui se présente en avant de l’être.</li>

<li>L’œil a la puissance de devancer la lumière pour façonner le feu en une révolution.</li>

<li>L’œil attise l’autonomie de sa puissance lorsqu’il accepte qu’elle se situe en l’obscur.</li>

<li>L’œil obscur devient la lumière qui anarchitecture la cité par sa pratique dialectique.</li>
</ol>
