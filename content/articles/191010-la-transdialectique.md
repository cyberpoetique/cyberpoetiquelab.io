---
title: "La transdialectique"
mot: "trans"
date: "2019-10-10"
description: "La transdialectique est une surrection des horizontalités. Elle déploie un espace du devenir où la dialectique se présente comme une affirmation sensorielle délaissant toute croyance en une quelconque grammaire."
images:
  - "img/191010-la-transdialectique.jpg"
sourceimage: "Schiele, Herbstbaum in bewegter Luft (Winterbaum) / https://commons.wikimedia.org/wiki/File:Egon_Schiele_-_Autumn_Tree_in_Stirred_Air_(Winter_Tree)_-_Google_Art_Project.jpg"
persona:
  - "Le Réseaü"
espace:
  - "fabrica.f"
echo:
  - "microantilivre"
  - "praxis"
  - "transdialectique"
---

C'est avec ce *microantilivre* que nous laissons émerger des réseaux un monde parallèle au nôtre, qui grouille d'un rêve de renversement et se nourrit de toute information pour augmenter sa puissance ontologico-politique et développer sa méthode du devenir : <a href="https://www.transdialectique.org">la transdialectique</a>.

*Ce* [microantilivre](https://www.antilivre.org/#microantilivre) *est disponible dans sa* <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_la_transdialectique.pdf">version PDF</a> *ainsi que dans sa* <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_la_transdialectique_imprimable.pdf">version imprimable DIY.</a>

<h1 class="ligneblanche centre"><span class="glitch" data-text="La transdialectique">La transdialectique</h1>
<h2 class="centre ligneblanche"><span class="glitch" data-text="Le Réseaü">Le Réseaü</h2>

<p class="epigraphe">Que vive l'ontologie politique !<br>
un·e passant·e cagoulé·e, plusieurs</p>

La transdialectique est une surrection des horizontalités. Elle déploie un espace du devenir où la dialectique se présente comme une affirmation sensorielle délaissant toute croyance en une quelconque grammaire.

Au-delà des apparences closes de la raison qui impose l’idée de vérité telle une réalité indépassable, au-delà des miroitements de valeurs chancelantes qui continuent à éblouir jusqu’à l’étourdissement une société du spectacle centrée sur la domination individuelle, la transdialectique émerge des formes réticulaires de la technique et renverse avec fracas l’établi moderne. Elle menace l’humain qui se menace lui-même, et c’est la recherche de la vérité qui ne s’y recherche plus, cette quête folle qui caractérise la pensée humaine dans sa violence impératrice. Mue par l’empreinte de *l’ego-roi*, cette recherche a fabriqué une course à la domination individuelle comme moteur premier de la modernité.

Mais la transdialectique s’arme contre la modernité ! Elle invoque d’antiques spectres, les tragiques, se donne pour tâche d’abattre les hiérarchies individuantes, trame un renouveau acentré des valeurs, dont la substance plurielle se rapprocherait des échos de la *physique*, cette totalité embrassant le réel et laissant paraître des champs obscurs au-delà du réel lui-même. La multiplication des perspectives sur cette *physique* pressent un dépassement de la vérité qui s’entend comme le dépassement de l’humain. La pratique de ces perspectives, foisonnantes, infinies et sinueuses, déconstruit toute vérité empesant la pensée, et esquisse un entrelacs sensoriel où le devenir électrique de l’humain révèle un sujet réticulaire, une multitude autonome se confondant à son devenir spatial. Cette multitude se définit par l’unité de sa subjectivité, tel un réseau dont les entités s’autoréguleraient en permanence au fil de son accroissement.

La transdialectique réévalue les valeurs matérialistes de l’*Aufhebung* et suggère une pratique de la *Durch-hebung*, de la *trans-surrection* afin d’exprimer la volonté d’horizontaliser la dynamique dialectique. Elle vise à en faire une valeur mouvante propre à la *physique* par une transmutation hyperfréquentielle de celle-ci. C’est cette hyperfréquence du changement qui permet, par une pratique perspectiviste, de sonder ontologiquement et politiquement la *physique*. La réévaluation permanente en tant que changement permanent de perspective sur la *physique* structure un réseau de valeurs mouvant se rapprochant toujours plus de l’essence physicaliste de l’espace qui s’étend au-devant de la multitude. Ce rapprochement s’effectue sans jamais désirer la jonction de cette essence, qui demeure un but impossible lorsque l’essence elle-même forme un perpétuel mouvement. Seule la vectorisation parmi le champ de ce mouvement physique importe.

Cette recherche ne prétend pourtant à aucun absolu puisque le mouvement de l’espace structurant l’être en un devenir ontologique empêche de figer une quelconque essence. Si cette essence doit être, elle n’est que récursivité de son propre mouvement. L’essence devient son devenir transformateur. S’ajoute à cette impossibilité de fixation l’étroitesse de l’esprit humain contraint par les limites du langage, de cette accumulation animale de signes qui s’évertue péniblement à cristalliser le devenir mouvant de l’être. La transdialectique admet les errances langagières, et ne tentent pas de faire fi de la force de l’image, mais, au contraire, de porter l’image jusqu’à la transe qui laisse sentir *l’après-langage*.

{{< figurelazy src="/img/191010-la-transdialectique-3.jpg" alt="La transdialectique" width="50%" class="figure--centre" >}}

La transdialectique présage la découverte de l’indistinction du sujet et de l’objet. Elle dépasse l’errance grammaticale qui disjoint le verbe du sujet agissant et qui masque l’indifférence entre l’acteur, son action et l’espace où elle se porte. Cet augure annonce la libération de l’humain de sa propre individuation, de son cloisonnement en une socialité qui refuse de se structurer dans une continuelle réévaluation des valeurs, dont la première reste, siècle après siècle, le mortifère *logos*.

Contre la force négative de la dialectique, la transdialectique pose une dialectique affirmative, où n’existe aucune différence entre le sujet et l’objet, aucune indifférence, mais une communion où ne peut même s’envisager l’énonciation de l’indifférence de ces deux entités. Cette communion est l’affirmation du devenir *physique*, où la pluralité inextricable de sa composition s’esquisse en un seul mouvement subjectif de l’espace.

Cette subjectivité spatiale se meut par une transmutation autonome qui s’opère par la confrontation aux éléments qui se présentent à sa spatialité, et qui, par cette confrontation, viennent s’y intégrer indissociablement pour s’accroître avec elle. Ici vacille l’image tantôt entre la croissance végétale qui exprime un cycle continuel de la transformation, de la régénérescence et de l’expansion, tantôt celle des obscurités de l’univers où l’accélération de l’expansion de l’espace devient son essence même. L’entropie s’y fait affirmation de l’être en se nourrissant des transformations de l’espace. Le désordre devient ainsi une croissance, le bariolé une condition de l’unité.

La transdialectique se situe à un niveau ontologique, mais elle y laisse émerger une politique issue de cette surrection des horizontalités. Elle ne dissocie pas son expression ontologique de son expression politique, sa dynamique ne pouvant être qu’une ontologie politique. La démocratie devient de la sorte à la fois l’ennemi, lorsqu’elle se contente d’être un régime politique où la somme des individuations architecture des hiérarchies et menace ce qui exprime une pluralité mouvante de l’être, et la condition consubstantielle de cette ontologie du devenir spatial, lorsque la démocratie est l’expression radicale du pouvoir de la multitude sur l’espace de son devenir. La démocratie radicale se manifeste alors comme la substance de la mécanique du sujet réticulaire. L’autonomie y est loi. Elle empêche l’écrasement hiérarchique des marges en distribuant de manière acentrée le pouvoir décisionnel, et en s’attaquant aux valeurs individuelles dont le centre spectaculaire réside dans la valeur de propriété privée.

En ce système cybernétique, où l’importance ne va plus aux pôles du système, mais à la communication acentrée entre ces pôles, le surhumain émerge non pas contre la dialectique, mais par la dialectique, il devient un sujet réticulaire niant toute entité individuelle, toute domination et faisant corps avec l’objet environnemental de sa subjectivité. Il se fait ainsi affirmation du commun, et surgit comme une métamorphose qui bascule vers un transhumanisme anarchiste et communiste. Par son autonomie, ce transhumanisme exprime une force révolutionnaire forte de ses rêves d’aurores nouvelles, et anime la lutte contre la modernité de flamboiements qui figurent le partage de la matière et l’action harmonieuse du sujet au sein de son environnement.

La confrontation n’est plus simplement politique, mais elle est une totalité ontologico-politique. Cette confrontation avec l’espace de son devenir façonne le sujet réticulaire en un sujet révolutionnaire. Ce sujet n’évolue plus en une simple spatialité, il est l’espace qui devient, une expansion où l’ontologie et la politique s’unissent dans l’affirmation d’une multitude indivisible, bariolée et autonome. Grâce à la transdialectique, ce sujet détruit l’idée de verticalité pour construire, par l’entremise de l’idée d’une horizontalité multiple, celle d’un espace non orthogonal du devenir. La transdialectique exprime à la fois la *physique* et l’outil de sa conquête, elle est un éternel passage, un éternel devenir, et compose un réseau subjectif en constante transmutation, tel un monde dont la gloire de l’éternelle révolution embrase d’espérances l’espace à venir.
