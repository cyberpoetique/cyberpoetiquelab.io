---
title: "Blanche Acétylène&nbsp;!"
title: "Blanche Acétylène"
mot: "acétylène"
date: "2020-02-20"
description: "Le métier craque, la navette vole, Et jour et nuit, nous tissons sans paroles &mdash; Vieille Allemagne, nous tissons ton sindon, Y tissons la triple malédiction &mdash; Nous tissons, nous tissons !"
images:
  - "img/200220-blanche-acetylene.jpg"
sourceimage: "Franz Marc, Kämpfende Formen / https://commons.wikimedia.org/wiki/File:Marc_-_K%C3%A4mpfende_Formen_PA291382.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "fabrica.f"
echo:
  - "placard"
  - "Vaché"
  - "dada"
script: |
  // Source inspiration : a pen by Milica, codepen.io/micikato/pen/PpaXMb
  var hero = document.querySelector('.ombresmouvantes');
  var text = hero.querySelector('.ombresmouvantes__texte');
  var walk = 100; // px
  
  function shadow(e) {
  
  var width = hero.offsetWidth, height = hero.offsetHeight;
  var x = e.offsetX, y = e.offsetY;
  
    if (this !== e.target) {
        x = x + e.target.offsetLeft;
        y = y + e.target.offsetTop;
    }
  
    var xWalk = Math.round(x / width * walk - walk / 2);
    var yWalk = Math.round(y / height * walk - walk / 2);
  
    text.style.textShadow = '\n      ' +
    xWalk + 'px ' + yWalk + 'px 0 rgba(0,0,0,0.05),\n      ' +
    xWalk * -1 + 'px ' + yWalk + 'px 0 rgba(0,0,0,0.07),\n      ' +
    yWalk + 'px ' + xWalk * -1 + 'px 0 rgba(0,0,0,0.09),\n      ' +
    yWalk * -1 + 'px ' + xWalk + 'px 0 rgba(0,0,0,0.11)\n    ';
  
  }
  
  hero.addEventListener('mousemove', shadow);
---

Le 26 novembre 1918. Ô la putain de guerre, t'as raté Jack ! Voilà Vaché tout à sa fureur. Et avec lui, la *blanche acétylène* plein les narines, on se faufile parmi nos *beaux whiskys*, ça se pâme. Embrasements. Eh quoi, le siècle à la renverse.

*Blanche Acétylène !* Ou le texte bath, dont les encres se picolent au front, entre deux obus. Et *l'irisé* qui s'affiche sous la forme d'un [placard abrupt](https://www.antilivre.org/#placard), disponible dans son format original A3 pour être <a href="https://txt.abrupt.cc/antilivre/placard_abrupt_vache_jacques_blanche_acetylene.pdf">lu, imprimé, placardé.</a>

Ta main, Harry, elle a plié novembre ! Et ta correspondance qui se fait prophétesse des Années folles :

- au vieux Lewis : "--- La guerre va, paraît-il, finir --- Ce me semble un peu mythique --- et puis, au milieu de la mauvaise saison, c'est ennuyeux --- Mais qu'y faire ?"
- au paternel : "Ouf ! Il me semble que voilà l'onomatopée qui traduit le mieux le sentiment d'une foule de gens sur l'armistice --- et le mien. J'aurais toutefois cru le vieux Kaiser plus beau joueur vers la fin de la partie ---"
- au général en chef des armées (André Breton) : "Dans quel affalement me trouva votre lettre ! --- Je suis vide d'idées, et peu sonore, plus que jamais sans doute enregistreur inconscient de beaucoup de choses, en bloc --- Quelle cristallisation ?... je sortirai de la guerre doucement gâteux, peut-être bien, à la manière de ces splendides idiots de village (et je le souhaite)... ou bien... ou bien... quel film je jouerai ! --- Avec des automobiles folles, savez-vous bien, des ponts qui cèdent, et des mains majuscules qui rampent sur l'écran vers quel document !... Inutile et inappréciable ! --- Avec des colloques si tragiques, en habit de soirée, derrière le palmier qui écoute ! --- Et puis Charlie, naturellement, qui rictusse, les prunelles paisibles. Le Policeman qui est oublié dans la malle ! ! ---"

{{< figurelazy src="/img/200220-blanche-acetylene-1.jpg" alt="Blanche acétylène" width="50%" class="figure--centre" >}}

&nbsp;<br>

<h1 class="ligneblanche centre"><span class="glitch" data-text="Jacques Vaché">Jacques Vaché</span></h1>
<h2 class="centre ligneblanche"><span class="glitch" data-text="Blanche Acétylène !">Blanche Acétylène !</span></h2>


<div class="ombresmouvantes">

<div class="ombresmouvantes__texte">

26 novembre 1918

<p class="centre">&mdash; Blanche Acétylène !</p>

<p>Vous tous ! &mdash; Mes beaux whiskys &mdash; Mon horrible mixture ruisselant jaune &mdash; bocal de pharmacie &mdash; Ma chartreuse verte &mdash; Citrin &mdash; Rose ému de Carthame &mdash;</p>

<p><span style="display:inline-block;width:3em;"></span>Fume !</p>

<p><span style="display:inline-block;width:6em;"></span>Fume !</p>

<p><span style="display:inline-block;width:9em;"></span>Fume !</p>

<p>Angusture &mdash; noix vomique et l'incertitude des sirops &mdash; Je suis un mosaïste.</p>

<p>...«&nbsp;Say, Waiter &mdash; You are a damn'fraud, you are &mdash;&nbsp;»</p>

<p>Voyez-moi l'abcès sanglant de ce prairial oyster ; son œil noyé me regarde comme une pièce anatomique ; Le barman me regarde peut-être aussi, poché sous les globes oculaires, versant l'irisé, en nappe, dans l'arc-en-ciel.</p>

<p>OR</p>

<p>l'homme à tête de poisson mort laisse pendre son cigare mouillé. Ce gilet écossais ! &mdash;</p>

<p><span style="display:inline-block;width:6em;"></span>&mdash; L'officier orné de croix &mdash; La femme molle poudrée blanche bâille, bâille, et suce une lotion capillaire &mdash; (ceci pour l'amour.).</p>

<p><span style="display:inline-block;width:6em;"></span>&mdash;&nbsp;«&nbsp;Ces créatures dansent depuis neuf heures, Monsieur.&nbsp;» &mdash; Comme ce doit être gras &mdash; (ceci pour l'érotisme, voyez-vous.).</p>

<p>&mdash; alcools qui serpentent, bleuis, somnolent, descendent, rôdent, s'éteignent.</p>

<p><span style="display:inline-block;width:3em;"></span>Flambe !</p>

<p><span style="display:inline-block;width:6em;"></span>Flambe !</p>

<p><span style="display:inline-block;width:9em;"></span>Flambe !</p>

<p class="centre">MON APOPLEXIE !!</p>

<p>N.B. &mdash; Les lois, toutefois, s'opposent à l'homicide volontaire &mdash; (et ceci pour morale... sans doute ?).</p>

<p class="droite">(.<em>Harry James</em>.)</p>

</div>

</div>

{{< figurelazy src="/img/200220-blanche-acetylene-2.jpg" alt="Blanche acétylène" width="50%" class="figure--centre" >}}
