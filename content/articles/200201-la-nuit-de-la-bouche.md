---
title: "La nuit de la bouche"
mot: "nuit"
date: "2020-02-01"
images:
  - "img/200201-la-nuit-de-la-bouche.jpg"
sourceimage: "Paul Klee, Engle Anwärter / https://commons.wikimedia.org/wiki/File:Paul_Klee_~_Engel_Anw%C3%A4rter_~_1939.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "vidéoème.v"
echo:
  - "graphique"
  - "antilivre"
  - "vidéo"
---

*La nuit est une bouche, toutes les nuits sont le corps de l’autre, et dans la bouche, il y a la nuit qui se traverse, le goût des nuits qui souffle et les ombres et les ciels, de la bouche à la bouche, le corps de l’autre qui se cristallise, sa nuit qui s’en va à la rencontre.*

{{< youtube vhebT4jagE4 >}}

[La nuit de la bouche](https://abrupt.cc/etienne-michelet/la-nuit-de-la-bouche/), un antilivre [d'Étienne Michelet](https://www.dongmuri.net/).

*(Le texte lu et la vidéo sont mis à disposition selon les termes de la Licence CC-BY-NC-SA 4.0. Nous avons néanmoins une lecture [adaptative](https://abrupt.cc/partage) de cette licence.)*
