---
title: "Iconostase&nbsp;I"
slug: "Iconostase I"
mot: "icônes"
date: "2020-02-02"
images:
  - "img/200202-iconostase-1.jpg"
sourceimage: "Liubov Sergeievna Popova, Composition (Red-Black-Gold) / https://commons.wikimedia.org/wiki/File:Liubov_Sergeievna_Popova_-_Composition_(Red-Black-Gold)_-_Google_Art_Project.jpg"
persona:
  - "Esperanza Rojo"
espace:
  - "pictura.py"
echo:
  - "graphique"
  - "iconostase"
library:
  - "vanilla-tilt.min.js"
script: |
  VanillaTilt.init(document.querySelectorAll(".tilt"), {
    perspective: 700,
    speed: 100,
    glare: true,
    "max-glare": 0.2,
   gyroscopeMinAngleX: -5,
    gyroscopeMaxAngleX: 5, 
    gyroscopeMinAngleY: -5,
    gyroscopeMaxAngleY: 5,
  });
---

<p class="italique droite">(dresser-des-contre-icônes)</p>
<p class="italique gauche">(tout-contre-l'im-monde)</p>
<p class="italique centre">(y-mur-murer-nos-contre-récits)</p>

<br>


{{< figureglitch src="/img/200202-christ.jpg" alt="c.h.r.i.s.t." width="70%" margin="0 0 0 auto" class="tilt" >}}
{{< figureglitch src="/img/200202-libido.jpg" alt="l.i.b.i.d.o." width="70%" margin="0 auto 0 0" class="tilt" >}}
{{< figureglitch src="/img/200202-mosette.jpg" alt="m.o.s.e.t.te" width="70%" margin="0 auto" class="tilt" >}}
