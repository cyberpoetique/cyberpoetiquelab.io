---
title: "Une parallaxe chiffre sa voyance"
mot: "parallaxe"
date: "2018-09-28"
type: "log"
description: "Il n’est pas ici question d’apprentissage profond, de ses conséquences merveilleuses sur la création artistique, d’un chamanisme des robots qui structurerait les plans d’une théorie animiste parmi le numérique — nous y reviendrons. Peut-être de réseau neuronal convolutif ? Non. Qu’y comprendrions-nous, qu’y fomenterions-nous ? Il n’est question que d’un léger exercice de parallaxe."
images:
  - "img/180928-parallaxe.jpg"
sourceimage: "Rozanova, Non-Objective Composition (Suprematism) / https://commons.wikimedia.org/wiki/File:Non-Objective_Composition_(Suprematism)_(Rozanova,_1916-1917)_-_anagoria.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "futurologie"
  - "rêve"
  - "robot"
---

Il n’est pas ici question <a href="https://fr.wikipedia.org/wiki/Apprentissage_profond" target="_blank">d’apprentissage profond,</a> de ses conséquences merveilleuses sur la création artistique, d’un chamanisme des robots qui structurerait les plans d’une théorie animiste parmi le numérique — nous y reviendrons. Peut-être de <a href="https://fr.wikipedia.org/wiki/R%C3%A9seau_neuronal_convolutif" target="_blank">réseau neuronal convolutif ?</a> Non. Qu’y comprendrions-nous, qu’y fomenterions-nous ? Il n’est question que d’un léger exercice de parallaxe. Tenter de jeter hors de soi, du carcan corporel, biologique, le phénomène perceptif. Nous voyons, nos êtres globuleux perçoivent : l’image est lisse, immédiate, et le réel pourtant si opaque — ou n’est-ce notre esprit si opaque au réel ? La réponse que nous apportons à cette dernière question — un esprit opaque qui clôt le réel — nous force à tenter de complexifier nos sens perceptifs, de les établir comme des simplifications, des mensonges, des barrières ; il nous faut multiplier les perspectives, déterrer l’information perceptive, la moelle de notre réel subjectif. La parallaxe, ici: une focale sur notre focale, décomposer l’œil, sortir de son système nerveux, voir sans le nerf optique, percevoir avec une recomposition de la vision, et puis recomposer l’œil composé, de l’insecte à l’humain, et notre œil recomposé vers la machine, l’intelligente qui ne se contente jamais de l’image lisse, immédiate, mais n’y découvre que le médium d’une information qui s’y dissimule. Les ordinateurs commencent à ressentir le monde, ils voient, transcrivent le réel, le lisse, l’immédiat en binaire, en strates d’informations --- il nous faut être à leur suite. Ils calculent leurs perceptions, et sentent ce qui se meut sous le réel. Ils s’avancent au-delà des calculs vers l’émotion brute que cache le traitement de l’information. Alors sans yeux, sans biologie, avec chiffre et émotion, quelle voyance ? Quel chiffre pour leur voyance ? Et la nôtre, la future, l’obscure ? Voir le monde sans se contenter de ses surfaces, n’y voir que l’entrelacs d’informations circulantes.
