﻿---
title: "Des écrans-feuilles au service de l’hypertextualité"
mot: "écrans-feuilles"
date: "2018-09-10"
description: "Et si le livre numérique n’était que le cadavre qui patientait sagement dans l’attente de son Docteur Frankenstein ? Un monstre dont la renaissance illuminerait la potentialité textuelle ? Que le rapiècement se fasse avec des écrans nouveaux."
images:
  - "img/180910-ecrans-feuilles.jpg"
sourceimage: "Lissitzky, Proun 19D / https://commons.wikimedia.org/wiki/File:NY_Moma_lissitsky_19D.JPG"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "futurologie"
---

Et si le livre numérique n’était que le cadavre qui patientait sagement dans l’attente de son Docteur Frankenstein ? Un monstre dont la renaissance illuminerait la potentialité textuelle ? Que le rapiècement se fasse avec des écrans nouveaux. Après avoir longuement mâché de vieux morceaux de silicium, nous avons vu avec effroi notre possible futur, une dystopie autoritaire, mais également des outils de résistance, des technologies inconnues, des écrans pliables, roulables, pas plus épais que l’antique feuille, dont la structure modulaire offrait à l’usager la possibilité de les relier en un assemblage numérique de plusieurs centaines d’écrans-feuilles. Au choix : de l’encre électronique ou des diodes organiques, une surface réflective ou l’éclat des couleurs.

À l’inverse d’autres arts comme la musique ou le cinéma qui ont su non seulement s’adapter aux technologies nouvelles mais s’en servir pour croître, la littérature, quant à elle, souffre certes de son milieu ronronnant un peu sénile dans son tweed, mais aussi d’une technologie bidimensionnelle --- les écrans de nos tablettes et téléphones --- qui ne reproduit le confort tridimensionnel du livre. Là se cristallise l’attente du cadavre. Il rêve de sa tridimensionnalité numérique. Et notre vision porte justement sur une technologie qui rendrait compte des dimensions plurielles de l’objet lu, une chose adaptative, volumique : volumen ou rotulus, codex si nous le souhaitons. Avec ces écrans-feuilles reliables de diverses façons, légers et flexibles, l’objet livresque devancerait les vieilles formes papelardes, apporterait une accélération supplémentaire et physique à l’hypertextualité.
