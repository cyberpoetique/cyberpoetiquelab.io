﻿---
title: "Des tableaux et des boucles aux ombres d’infini"
mot: "infini"
date: "2018-09-11"
description: "Prenons un tableau, dans son sens informatique, c’est-à-dire une structure de données, contenant tous les mots d’une langue. Cette dernière deviendrait un vecteur unidimensionnel composé d’un grand nombre, mais d’un nombre limité de cases : pour chaque mot existant, une case."
images:
  - "img/180911-tableau-boucles.jpg"
sourceimage: "Mondrian, Composition / https://commons.wikimedia.org/wiki/File:Composition,_Piet_Mondrian,_1916.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "exhortation"
  - "code"
---

Prenons un tableau, dans son <a href="https://fr.wikipedia.org/wiki/Tableau_(structure_de_donn%C3%A9es)" target="_blank">sens informatique</a>, c’est-à-dire une structure de données, contenant tous les mots d’une langue. Cette dernière deviendrait un vecteur unidimensionnel composé d’un grand nombre, mais d’un nombre limité de cases : pour chaque mot existant, une case. Une langue d’une centaine de milliers de mots serait donc composée d’une centaine de milliers de cases. Et à partir de là, dans l’oubli des scléroses et des grammaires, réordonnons, désordonnons l’ordre des cases, composons les listes possibles, toutes les listes ! Il y aurait factorielle d’une centaine de milliers de listes possibles, en somme 2,824 229 408 fois 10 à la puissance 456 573 de listes (approximativement). Et le tout multiplié évidemment par la centaine de milliers de mots de la langue donnée afin d’avoir le mesure du nombre de cases, du nombre de mots à dire si ces listes ensemble se voulaient un seul poème.

À partir de cette structure langagière, à vrai dire un simple dictionnaire, laissons-nous à ce songe de la récitation de tous les mots de chacune de ces listes. Si l’ordre a une quelconque importance en poésie, et que nous y façonnons sans cesse des brisures tantôt syntaxiques, tantôt sémantiques, il paraît rassurant de percevoir dans notre précarité animale les ombres d’infini.

Ce même tableau pourrait plus aisément être utilisé en une <a href="https://fr.wikipedia.org/wiki/Structure_de_contr%C3%B4le#Boucles" target="_blank">boucle</a> où une VARIABLE s’imprimerait différemment à chaque itération, jusqu’à épuisement de toutes les cases du tableau :

*Si la grammaire est un mensonge, il sied d’abolir le mot « VARIABLE ».*

Et nous retournerons ainsi à une communication libérée du langage.

(Cette idée est une invitation.)
