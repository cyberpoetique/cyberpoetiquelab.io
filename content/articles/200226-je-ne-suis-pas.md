---
title: "Je ne suis pas"
mot: "être"
date: "2020-02-26"
description: "Moi, je suis chevalier d’industrie. Je suis marin sur le Pacifique. Je suis muletier. Je suis cueilleur d’oranges en Californie. Je suis charmeur de serpents. Je suis rat d’hôtel. Je suis neveu d’Oscar Wilde. Je suis bûcheron dans les forêts géantes. Je suis ex-champion de France de boxe. Je suis petit-fils du chancelier de la reine. Je suis chauffeur d’automobile à Berlin. Je suis cambrioleur. Je suis etc., etc., etc."
images:
  - "img/200226-je-ne-suis-pas.jpg"
sourceimage: "Robert Delaunay, Carousel des cochons / https://commons.wikimedia.org/wiki/File:Robert_delaunay,_caroselli_di_maiali,_1922,_01.JPG"
persona:
  - "La Cyberpoétique"
espace:
  - "codex.cpp"
echo:
  - "Vaché"
  - "Cravan"
  - "dada"
library:
  - "sortable.min.js"
  - "baffle.min.js"
script: |
  var container = document.querySelector('.sortable');
  var sortable = Sortable.create(container, {
    animation: 500,
    draggable: ".element",
    fallbackTolerance: 3,
    ghostClass: 'element--move',
  });
  
  const btnShuffle = document.querySelector('.shuffle');
  const poeme = document.querySelector('.sortable');

  let v = baffle('.sortable .element')
    .reveal(500)
    .set({
      characters: 'arthurcravanjacquesvaché',
      speed: 50
    });

  btnShuffle.addEventListener('click', (e) => {
    for (let i = poeme.children.length; i >= 0; i--) {
        poeme.appendChild(poeme.children[Math.random() * i | 0]);
    }
    v.reveal(500);
  });
---

Voici un inédit ! Un texte composé par Arthur Cravan et Jacques Vaché au printemps 1921, qui fut psalmodié lors de leurs incantations sylvestres. Comme chacun sait, Cravan et Vaché sont partis ensemble, à l’hiver 1919, vers l’Oural des Révolutions, au cœur des terres des Oudmourtes, dans l’espoir d’y marier leur communalisme poétique à un paganisme ancestral. Nous défendons l’héritage de leurs danses chamaniques qui se retrouve dans les saccades de nos réseaux informatiques, et nous vous proposons ici la trace d’un de leurs chants.

<p class="pasdeligneblanche">Ces paroles n’attendent que votre participation.</p>
<p class="pasdeligneblanche italique">(Survolez, déplacez et agitez le verbe.)</p>

<button class="pasdeligneblanche btn--submit shake shuffle" type="submit" id="submit">Dada !</button>

<div class="nohyphen sortable">

<p class="element">Je suis en prison, naturellement</p>

<p class="element">J’ai 34 ans et je suis cigare</p>

<p class="element">Je suis descendu dans mon ventre, et je dois commencer à être dans un état féerique</p>

<p class="element">Mon tube digestif est suggestif</p>

<p class="element">J’ai peut-être 17 ans mais je suis villa</p>

<p class="element">Je crois bien que j’étais mort</p>

<p class="element">On va dire que j’étais ivre-mort</p>

<p class="element">Et voilà que je suis triste et plein d’amour</p>

<p class="element">Maintenant</p>

<p class="element">I am dry</p>

<p class="element">Mais avant tout je suis de moi-même l’ami</p>

<p class="element">Aussi vrai que je suis rieur</p>

<p class="element">Je suis redevenu un homme</p>

<p class="element">Étant bien rasé, je suis de si belles mains à solitaire</p>

<p class="element">Et je serai gentil avec toi et nous irons partout</p>

<p class="element">Car je suis tes gammes et tes couleurs</p>

<p class="element">Je suis un mosaïste</p>

<p class="element">Je suis toutes les choses, tous les hommes et tous les animaux !</p>

<p class="element">Mais je suis ici, sur ce lit, comme un fainéant</p>

<p class="element">Je ne peux plus être épicier pour l’instant &mdash; l’essai fut sans succès heureux</p>

<p class="element">Je suis très fatigué de médiocres</p>

<p class="element">Et je ne suis pas artiste peintre</p>

<p class="element">Oh ! chochotte ! (ta gueule !) La peinture c’est marcher, courir, boire, manger, dormir et faire ses besoins. Vous aurez beau dire que je suis un dégueulasse, c’est tout ça</p>

<p class="element">Je ne suis pas homme de lettres, jamais</p>

<p class="element">Et je ne suis surtout pas chevalier de la Légion d’honneur</p>

<p class="element">Moi, je suis une vache</p>

<p class="element">Moi, je suis chevalier d’industrie</p>

<p class="element">Je suis marin sur le Pacifique</p>

<p class="element">Je suis muletier</p>

<p class="element">Je suis cueilleur d’oranges en Californie</p>

<p class="element">Je suis charmeur de serpents</p>

<p class="element">Je suis rat d’hôtel</p>

<p class="element">Je suis neveu d’Oscar Wilde</p>

<p class="element">Je suis bûcheron dans les forêts géantes</p>

<p class="element">Je suis ex-champion de France de boxe</p>

<p class="element">Je suis petit-fils du chancelier de la reine</p>

<p class="element">Je suis chauffeur d’automobile à Berlin</p>

<p class="element">Je suis cambrioleur</p>

<p class="element">Je suis etc., etc., etc.</p>

<p class="element">Mais surtout je suis un vilain monsieur</p>

<p class="element">Je suis donc interprète aux anglais</p>

<p class="element">&mdash; J’existe avec un officier américain qui apprend la guerre, mâche de la «&nbsp;gum&nbsp;» et m’amuse parfois</p>

<p class="element">Je suis très loin d’une foule de gens littéraires &mdash; même de Rimbaud</p>

<p class="element">L’art est une sottise &mdash; presque rien n’est une sottise &mdash; l’art doit être une chose drôle et un peu assommante &mdash; c’est tout</p>

<p class="element">Je suis heureux de vous savoir malade, mon cher ami, un peu</p>

<p class="element">Moi, je suis assez mal portant, vis dans un trou perdu entre des chicots d’arbres, calcinés et, périodiquement une sorte d’obus se traîne, parabolique, et tousse</p>

<p class="element">Je suis presque toujours en prison pour le moment, c’est, pour l’été, plus frais</p>

<p class="element">Je suis en consigne ici &mdash; dans l’attente de quelles nouvelles aventures ? &mdash; Pourvu qu’ils ne me tuent pas pendant qu’ils me tiennent ?... pauvres gens...</p>

<p class="element">Je suis on ne peut plus à bout...</p>

<p class="element">Je suis vide d’idées, et peu sonore</p>

<p class="element">Je suis trappeur</p>

<p class="element">Je suis ou voleur, ou chercheur, ou chasseur, ou mineur, ou sondeur</p>

<p class="element">Je suis un bar de l’Arizona (Whisky &mdash; Gin and mixed ?)</p>

<p class="element">Je suis de belles forêts exploitables, et vous savez ces belles culottes de cheval à pistolet-mitrailleuse, j’en suis aussi</p>

<p class="element">Mais à jamais je demeure votre serviteur</p>

<p class="element">Et tout ça finira par un incendie, je vous dis, ou dans un salon, richesse faite. &mdash; Well.&nbsp;&mdash;</p>

</div>
