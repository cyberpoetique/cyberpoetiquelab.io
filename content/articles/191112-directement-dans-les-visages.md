---
title: "Directement dans les visages"
mot: "visages"
date: "2019-11-12"
description: "Nous vivons à l’ère de la profusion, de l’accumulation, de
l’hypercapture des attentions, de l’accélération exponentielle, et c’est
la langue de notre époque qu’il faut parler. La tentative doit exploser,
répondre à la surcharge par la surcharge, joindre toutes les idées,
toutes les théories, toutes les possibilités. Si nous voulons essayer de
partager quelque chose, il faut tenter de tout raconter, dire la
naissance du monde, la transformation générale et la victoire
insurrectionnelle."
images:
  - "img/191112-directement-dans-les-visages.jpg"
sourceimage: "Schiele, Mädchen mit Sonnenbrillen / https://commons.wikimedia.org/wiki/File:Egon_Schiele_-_M%C3%A4dchen_mit_Sonnenbrillen_-_1910.jpeg"
persona:
  - "Anthropie"
espace:
  - "fabrica.f"
echo:
  - "microantilivre"
  - "graphique"
  - "glitch"
  - "praxis"
  - "hypermodernité"
script: |
  // Source inspiration : a pen by Milica, codepen.io/micikato/pen/PpaXMb
  var hero = document.querySelector('.ombresmouvantes');
  var text = hero.querySelector('.ombresmouvantes__texte');
  var walk = 100; // px
  
  function shadow(e) {
  
  var width = hero.offsetWidth, height = hero.offsetHeight;
  var x = e.offsetX, y = e.offsetY;
  
    if (this !== e.target) {
        x = x + e.target.offsetLeft;
        y = y + e.target.offsetTop;
    }
  
    var xWalk = Math.round(x / width * walk - walk / 2);
    var yWalk = Math.round(y / height * walk - walk / 2);
  
    text.style.textShadow = '\n      ' +
    xWalk + 'px ' + yWalk + 'px 0 rgba(0,0,0,0.05),\n      ' +
    xWalk * -1 + 'px ' + yWalk + 'px 0 rgba(0,0,0,0.07),\n      ' +
    yWalk + 'px ' + xWalk * -1 + 'px 0 rgba(0,0,0,0.09),\n      ' +
    yWalk * -1 + 'px ' + xWalk + 'px 0 rgba(0,0,0,0.11)\n    ';
  
  }
  
  hero.addEventListener('mousemove', shadow);
---

[Anthropie](https://abrupt.cc/anthropie) hacke nos imaginaires et jette à nos cerveaux consumé(riste)s des rêves d'émeutes éclairées.

*Ce* [microantilivre](https://www.antilivre.org/#microantilivre) *autonomise la préface de* [Dio](https://abrupt.cc/anthropie/dio) *et nous invite à l'organisation. Il est disponible dans sa* <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_anthropie_directement_dans_les_visages.pdf">version PDF</a> *ainsi que dans sa* version imprimable <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_anthropie_directement_dans_les_visages_imprimable.pdf">DIY.</a>

<h1 class="ligneblanche centre"><span class="glitch--continu" data-text="Directement dans les visages">Directement dans les visages</h1>
<h2 class="centre ligneblanche"><span class="glitch--continu" data-text="Anthropie">Anthropie</h2>

<p class="epigraphe ivresse shake--continu">nous sommes tous·tes le fruit<br>des pierres qui sonnent le chaos</p>

<div class="ombresmouvantes">

<div class="ombresmouvantes__texte">

<p>Nous n’avons plus le temps.</p>

<p>Et qui croit encore que l’écriture puisse sauver quoi que ce soit ?</p>

<p>Mais par amour et un peu par impuissance aussi, on va quand même
essayer.</p>

<p>Avec nos sentiments, nos défaillances cognitives, nos culpabilités, nous
ne sommes pas à la hauteur de nos propres catastrophes, pas capables
d’affronter &mdash; même de comprendre &mdash; la violence de nos structures. Nous
sommes brisé·e·s, mentalement instables, au bord de l’extinction,
surveillé·e·s, guidé·e·s dans nos comportements, essoufflé·e·s dans nos
consciences, attaqué·e·s dans nos différences.</p>

<p>Notre urgence c’est de dire, de devenir frontal, d’inscrire les mots
directement dans les visages, d’essayer la médiation, de s’auto-éduquer
ensemble à identifier l’urgence.</p>

<p>Nous vivons à l’ère de la profusion, de l’accumulation, de
l’hypercapture des attentions, de l’accélération exponentielle, et c’est
la langue de notre époque qu’il faut parler. La tentative doit exploser,
répondre à la surcharge par la surcharge, joindre toutes les idées,
toutes les théories, toutes les possibilités. Si nous voulons essayer de
partager quelque chose, il faut tenter de tout raconter, dire la
naissance du monde, la transformation générale et la victoire
insurrectionnelle.</p>

<p>Notre seul espoir de faire communauté, c’est le foisonnement, c’est la
vitesse, c’est le désordre.</p>

{{< figurelazy src="/img/191112-directement-dans-les-visages-3.gif" alt="La transdialectique" width="50%" class="figure--centre" >}}

<p>Il n’y a plus le temps pour l’académisme de la transgression. Dans le
cocon du faux libertinage intellectuel, seule l’injonction morale choque
encore, et tout est moral, parce que tout est à refaire, tout est
politique, parce que personne n’est innocent.</p>

<p>Notre urgence c’est de réécrire les histoires, de déjouer les récits
dominants et d’inventer des contre-récits, de découdre les fils des
romans nationaux, de se réapproprier le <em>storytelling</em> comme instrument
de re-modélisation du monde, de partage et d’amour.</p>

<p>Il faut une écriture moralisatrice, une écriture qui dise quoi faire,
même si elle se plante, au moins elle aura essayé, elle n’aura pas fait
semblant, elle aura voulu miner les tours d’ivoire.</p>

<p>Il n’y a plus le temps pour la valorisation marchande, les artistes
rentables, la natation amère dans les rapports de production,
l’industrie créative, la commercialisation du <em>feel good</em>, la mise en
quantité, le chiffrage, la soumission à l’image de soi comme
marchandise.</p>

<p>Il faut faire dérailler les réseaux de communication, hacker les
machines et les esprits, déjouer l’hypervisibilité, rester dans l’ombre.
Il faut insérer cette puissance de déraillement dans la triade écran &ndash;
cerveau &ndash; logiciel.</p>

{{< figurehover src1="/img/191112-directement-dans-les-visages-1.jpg" src2="/img/191112-directement-dans-les-visages-2.jpg" alt="Directement dans les visages" width="100%" >}}

<p>Ce texte invoque les réseaux, il s'offre à tous·tes, et n'appartient à personne puisqu'il n'a pas de valeur. Il pourra toujours être réécrit, piraté et mis en téléchargement libre en d'obscurs endroits du cyberespace. Sa lecture n'est que transition, puisqu'il peut être transmis à tous·tes, en tous lieux, à tous instants. Et les conservateurices finiront par être dupé·e·s, puisque les réseaux que nous adorons dissimulent, derrière leur marchandisation fétichisée, une puissance séditieuse, celle de la multitude partageuse, dont nous nous faisons l'écho et qui transformera leurs cerveaux accapareurs en faisant d'elleux une ex-bourgeoisie révolutionnaire.</p>

<p>Insoumis·es à la forme, ensemble, nous composerons des langages autonomes et nouveaux qui triompheront et essaimeront le siècle en d'incessantes métamorphoses réticulaires.</p>

<p>Nous jetterons au-devant de nos transes électriques quelques lueurs sur l'essaim à venir que nous composerons tous·tes.</p>

</div>

</div>
