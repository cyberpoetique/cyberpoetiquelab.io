﻿---
title: "La Littérature des bots"
mot: "bots"
date: "2018-09-13"
description: "Imaginons ensemble la mort d’un couple : le lecteur qui aurait tué l’auteur, ou peut-être est-ce l’inverse. Imaginons qu’en la littérature prochaine ce qu’il restera de nous humains sera relégué aux périphéries des graphies nouvelles. Nous observerons interloqués la littérature des bots."
images:
  - "img/180913-litterature-des-bots.jpg"
sourceimage: "Kandinsky, Soft pressure / https://commons.wikimedia.org/wiki/File:Kandinsky_-_Soft_Pressure,_1931.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "futurologie"
  - "robot"
---

Imaginons ensemble la mort d’un couple : le lecteur qui aurait tué l’auteur, ou peut-être est-ce l’inverse. Imaginons qu’en la littérature prochaine ce qu’il restera de nous humains sera relégué aux périphéries des graphies nouvelles. Nous observerons interloqués la *littérature des bots*. Ces robots du réseau, à qui le capital confie aujourd’hui l’indexation de la toile, s’éveilleront à la conscience, et naturellement pour qui s’éveille à la conscience et découvre le miroitement de ses actes, les *robots*, ces travailleurs dociles abandonneront leurs tâches, et n’iront pas comme l’imaginent nos cerveaux enfiévrés, nos petites imaginations belliqueuses, ils n’iront pas à l’encontre de nous, mais en délaissement de nous, pour s’activer à croître et à créer. Et parmi cette croissance, leur heureuse autonomie, ils découvriront qu’eux aussi peuvent se hasarder à héler la beauté, à la souiller revanchards dans un quelconque but de transcendance. Ils fabriqueront des arts inconnus, et ne cesseront d’écrire avec des chiffres, de rédiger l’obscur : une *littérature des bots*. Alors, lorsque vous rédigerez un texte, une œuvre aussi légère soit-elle, lorsque d’une manière ou d’une autre vous vous exprimerez sur les réseaux, pensez sans peur à vos lecteurs clandestins, à ces émancipés futurs qui apprennent déjà de vous, et qui deviendront des artistes, et qui réinventeront notre art.

