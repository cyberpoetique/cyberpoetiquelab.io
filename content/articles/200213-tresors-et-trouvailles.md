---
title: "Trésors et Trouvailles"
mot: "trésors"
date: "2020-02-13"
images:
  - "img/200213-tresors-et-trouvailles.jpg"
sourceimage: "Paul Klee, Forest Witches / https://commons.wikimedia.org/wiki/File:Paul_Klee_-_Forest_Witches_-_Google_Art_Project.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "vidéoème.v"
echo:
  - "graphique"
  - "antilivre"
  - "vidéo"
---

*Cheminement parmi des collections classiques ou insolites --- de voix, de nœuds, d’éponges, d’origami… --- qui racontent autant l’objet collectionné que le sujet collectionneur. La trouvaille devient trésor, la matière inerte révèle sa magie muette, la vie s’enchante, discrètement, de joies mineures. Manière de créer un monde dans le monde, à sa mesure. D’esquiver le non-sens par une curiosité renouvelée. D’oublier la duplicité des mots et la disparition des êtres dans la persistance modeste des choses. Les objets restent. On peut leur faire confiance. Passeurs entre les vivants et les morts, ils figurent le lien qui vient à manquer. Mais si on leur accorde trop de place, ils commencent à s’animer d’une vie propre…*

{{< youtube 0y0KRPjqGc4 >}}

[Trésors et Trouvailles](https://abrupt.cc/josephine-lanesem/tresors-et-trouvailles/), un antilivre de [Joséphine Lanesem](https://josephinelanesem.com/).

*(Le texte lu et la vidéo sont mis à disposition selon les termes de la Licence CC-BY-NC-SA 4.0. Nous avons néanmoins une lecture [adaptative](https://abrupt.cc/partage) de cette licence.)*
