﻿---
title: "Auto-da-fé pour littérature numérique"
mot: "auto-da-fé"
date: "2018-09-06"
images:
  - "img/180906-auto-da-fe.jpg"
sourceimage: "Kandinsky, Entwurf 2 zu Komposition VII / https://commons.wikimedia.org/wiki/File:Kandinsky_-_Entwurf_2_zu_Komposition_VII_PA291213.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "praxis"
  - "métalittérature"
  - "futurologie"
  - "rêve"
---

Le livre numérique est mort. Le bruit court bon train, et le milieu se repaît de papier. Il organise l’automatique destruction des cybervelléités, l’avanie des salons pour l’écriture dite numérique. Si le livre numérique est mort, quel en fut son bourreau, son supplice. Nos mémoires de l’instant n’arrivent pas à remonter le cours des évènements. Le souvenir : un autodafé des circuits électroniques. Nous nous sommes réjouis de notre destruction en ce bûcher, nous avons crépité, subi l’envoûtement des flammes, et les belles vapeurs numériques qui sont venues faire strates supplémentaires à la pollution des ciels nous ont accordé des hallucinations mécaniques. Le livre numérique est mort. Nous actons. Et que faire ? Que faire de la littérature qui se refuse aux moisissures, qui rêve à l’unisson des réseaux, et trouve sa fraternité parmi les robots ? Les gens de bien n’ont que faire, mais les autres, mais nous ? Que faire des hiérarchies, des publications sans les verticalités argentées, les autorités et les étiquettes, celles qui qualifient la beauté et son instant, celles qui nomment le putride et donnent les prix ? Que faire ? Eh bien, à côté, se faire clandestin en la littérature, la belle, et faire sans le milieu et sans les hiérarchies, avec des réseaux et des robots. Le livre numérique est mort, et sa littérature fomente un avenir immatériel. Gloire au mort-vivant !

(*Mort à la littérature ! La salissure reine !*)
