---
title: "RIMBAUD.ZAP"
mot: "enfer"
date: "2020-05-20"
description: "Nous vous proposons d’écrire un livre collectif. Nous vous proposons de ne pas le signer. Nous vous proposons de le placer dans le domaine public volontaire. Nous vous proposons d’utiliser exclusivement des outils libres pour le réaliser. Nous vous proposons de revivre Une saison en enfer, de sonder le fantôme d’Arthur Rimbaud. Nous vous proposons de faire d’un monument un squat, de transformer la liturgie littéraire en une fête païenne. Nous vous proposons de placer nos technologies de l’information en « l’alchimie du verbe ». Nous vous proposons de nous servir de Git et de Gitter pour installer une ZAP (Zone Autonome à Poétiser)."
images:
  - "img/200520-une-saison-en-enfer.jpg"
sourceimage: "Heinrich Hoerle - Self-portrait in Front of Trees and Chimneys / https://commons.wikimedia.org/wiki/File:Worker_(Self-portrait_in_Front_of_Trees_and_Chimneys),_by_Heinrich_Hoerle,_1931,_oil_on_paper,_mounted_to_board_-_Busch-Reisinger_Museum_-_Harvard_University_-_DSC01628.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "fabrica.f"
echo:
  - "microantilivre"
  - "git"
  - "littérature"
  - "multitude"
  - "ZAP"
---

<h1 class="ligneblanche centre"><span class="glitch--continu" data-text="RIMBAUD.ZAP">RIMBAUD.ZAP</span></h1>

<p class="centre">«&nbsp;Il faut être absolument moderne.&nbsp;»</p>

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="Le préambule">Le préambule</span></h2>

Dans le cadre de [l’Open Publishing Fest](https://openpublishingfest.org/), nous proposons dès le vendredi 22 mai 2020, de tenir une ZAP (Zone Autonome à Poétiser), d'installer un squat dans le monument Rimbaud et d'y lancer une réécriture frénétique et collective de sa *Saison en enfer*. Tout y sera amour et irrévérence.

L'écriture de ce projet se déroule [ici](https://gitlab.com/antilivre/rimbaud.zap).<br>Pour en discuter, il existe un [chat public](https://gitter.im/antilivre/rimbaud.zap), visible pour tou·te·s.

<p class="pasdeligneblanche">Le texte de base d’Arthur Rimbaud, <em>Une saison en enfer</em> est également disponible dans son format de <a href="https://www.antilivre.org/#microantilivre">microantilivre</a>&nbsp;:</p>

- <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer.pdf">la version PDF</a>
- <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer_imprimable.pdf">la version imprimable DIY</a>
- <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_rimbaud_arthur_une_saison_en_enfer_ebook.epub">la version ebook</a>

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="La proposition">La proposition</span></h2>

- Nous vous proposons d’écrire un livre collectif.
- Nous vous proposons de ne pas le signer.
- Nous vous proposons de le placer dans le domaine public volontaire.
- Nous vous proposons d’utiliser exclusivement des outils libres pour le réaliser.
- Nous vous proposons de revivre *Une saison en enfer*, de sonder le fantôme d’Arthur Rimbaud.
- Nous vous proposons de faire d’un monument un squat, de transformer la liturgie littéraire en une fête païenne.
- Nous vous proposons de placer nos technologies de l’information en «&nbsp;l’alchimie du verbe&nbsp;».
- Nous vous proposons de nous servir de Git et de Gitter pour installer une ZAP (Zone Autonome à Poétiser).

Toute création est une recréation, toute invention est un vol.

Ensemble, créons, recréons, inventons et volons.

Ensemble, nous sommes Arthur Rimbaud.

Ensemble, nous sommes absolument modernes.

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="La participation">La participation</span></h2>

Comment participer ? Comment discuter ? Comment transmettre un texte ? Plusieurs possibilités :

- participer au [chat](https://gitter.im/antilivre/rimbaud.zap)
- utiliser la section *issues* comme un [forum](https://gitlab.com/antilivre/rimbaud.zap/issues)
- modifier directement le [texte](https://gitlab.com/antilivre/rimbaud.zap/blob/master/enfer.md) (il y a un bouton *Web IDE* pour ça)
- pour les personnes familières de Git, cloner le dépôt et envoyer un *merge request*
- pour les personnes ne souhaitant pas se créer de compte, nous envoyer votre contribution par email (ecrire [arobase] abrupt [point] ch)

Les mots, qui ne sont pas d'Arthur, sont indiqués en italiques (comme signalé ci-dessous, on n'y touche pas.)

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="Les règles">Les règles</span></h2>

Trois règles :

1. Comme dans le monde du graff, pas de *toying* ! On ne touche pas au désordre... on ne fait qu'ajouter du chaos au chaos. De manière pratique, les modifications ne visent que les parties qui ne sont pas en italiques, et les ajouts se glissent avant ou après les contributions en italiques déjà présentes.
2. Tenez-vous bien !
3. Ne respectez pas la règle 2.

(Modération mode Dzerjinski : *sommeil*.)

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="La licence">La licence</span></h2>

Ce projet est dédié au domaine public. Tous les textes ajoutés, ainsi que le code source (ne faisant pas partie d’une librairie préexistante), sont mis à disposition selon les termes de la Licence Creative&nbsp;Commons&nbsp;Zero (CC&nbsp;1.0 Universel) — voir à cet effet notre [politique](https://abrupt.cc/partage/) de partage.

<h2 class="ligneblanche centre"><span class="glitch--continu" data-text="Z comme Zbeul">Z comme Zbeul</span></h2>

(Cette ZAP se tiendra jusqu’à son *actualisation*.)

(Rimbaud est une ZAP.)
