---
title: "La Gittérature"
slug: "gitterature"
mot: "git"
date: "2020-05-14"
description: "La gittérature est l'enfantement de l'essaim qui s'échafaude par la circulation de l'information. La création y est re-création, récréation des formes qui se nourrissent d'elles-mêmes, dans la joie de voir se développer l'espace-temps de leur devenir."
images:
  - "img/200514-la-gitterature.jpg"
sourceimage: "El Lissitzky, Proposal for a PROUN street celebration / https://commons.wikimedia.org/wiki/File:El_Lissitzky_-_Proposal_for_a_PROUN_street_celebration_-_Google_Art_Project.jpg"
style:
  - "asciinema-player.css"
library:
  - "asciinema-player.js"
persona:
  - "La Cyberpoétique"
  - "Le Réseaü"
  - "Ann Persson"
espace:
  - "fabrica.f"
  - "codex.cpp"
echo:
  - "microantilivre"
  - "graphique"
  - "git"
  - "bash"
  - "script"
  - "littérature"
---

Du code qui fait vibrer la littérature jusqu'à son [hybridation](https://antilivre.gitlab.io/gitterature/).

Et trois textes, deux clandestins, cinq niveaux de lecture, du code et des outils libres, une théorie, des arts numériques, l'historicité d'une textualité, des boucles infinies, de l'erreur, du verbe électrique, et une brèche sur le monde qui vient. Voilà la *gittérature* qui se dévoile !

<p class="pasdeligneblanche">Les trois textes de ce projet sont disponibles en deux <a href="https://www.antilivre.org/#microantilivre">microantilivres</a>&nbsp;:</p>

- *la gittérature* : <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_la_gitterature.pdf">version PDF</a> et son format imprimable <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_le_reseau_la_gitterature_imprimable.pdf">DIY.</a>
- ou *la /g/lit/ch/térature* : <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_persson_ann_glitchterature.pdf">version PDF</a> et son format imprimable <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_persson_ann_glitchterature_imprimable.pdf">DIY.</a>

<asciinema-player src="/term/gitteraturechant.cast"></asciinema-player>

La [gittérature](https://gitlab.com/antilivre/gitterature/) est l'enfantement de l'essaim qui s'échafaude par la circulation de l'information. La création y est re-création, *récréation* des formes qui se nourrissent d'elles-mêmes, dans la joie de voir se développer l'espace-temps de leur devenir.

<asciinema-player src="/term/gitteratureprose.cast"></asciinema-player>

La *gittérature* est l'adjonction de la littérature aux potentialités de l'outil libre [Git](https://fr.wikipedia.org/wiki/Git). Nous saupoudrons sur cette rencontre quelques lignes de [Bash](https://fr.wikipedia.org/wiki/Bourne-Again_shell). (Pour un peu de *gittérature bashing...*)

<asciinema-player src="/term/gitteraturetheorie.cast"></asciinema-player>

Pour mettre en mouvement ce projet, il vous suffit d'installer [Git](https://git-scm.com/download) et de cloner ce dépôt :

```
git clone https://gitlab.com/antilivre/gitterature/
```

<asciinema-player src="/term/gitteraturefuite.cast"></asciinema-player>

Dès lors, il ne vous reste plus qu'à écrire une des [commandes disponibles](https://gitlab.com/antilivre/gitterature#commandes), dans un terminal, depuis le dossier "gitterature".

Et que la littérature soit l'irrévérence des réseaux !
