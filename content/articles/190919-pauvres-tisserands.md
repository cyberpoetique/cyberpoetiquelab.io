---
title: "Pauvres Tisserands&nbsp;!"
slug: "Pauvres Tisserands"
mot: "tisserands"
date: "2019-09-19"
description: "Le métier craque, la navette vole, Et jour et nuit, nous tissons sans paroles &mdash; Vieille Allemagne, nous tissons ton sindon, Y tissons la triple malédiction &mdash; Nous tissons, nous tissons !"
images:
  - "img/190919-pauvres-tisserands.jpg"
sourceimage: "Malevitch, Femme aux seaux / https://commons.wikimedia.org/wiki/File:Woman_with_Pails_(Malevich,_1912).jpg"
persona:
  - "Ann Persson"
espace:
  - "fabrica.f"
echo:
  - "placard"
  - "Heine"
  - "traduction"
---

Le poème *Die schlesischen Weber* (Les tisserands silésiens) de Heinrich Heine a paru pour la première fois le 10 juillet 1844 sous le titre *Die armen Weber* (Les pauvres tisserands) dans le journal *Vorwärts!*, édité notamment par Karl Marx. Le poème, qui figure sur cette page, a été revu par Heine en 1846. Il s'inscrit dans le cadre du *Vormärz*, mouvement littéraire politique, et tente de saisir la misère des tisserands en Silésie, qui se révoltèrent, durant l'année 1844, contre la violence imposée par l'industrialisation nouvelle.

Nous proposons ici une traduction d'Ann Persson, qui a pris le parti de respecter le rythme et la rime du poème allemand, en choisissant des décasyllabes ainsi que des rimes suivies. Cette traduction se place dans le domaine public volontaire, sous la licence Creative Commons Zero, comme le précise notre politique de [partage](https://abrupt.cc/partage).

Ce placard est disponible dans un format A3 recto-verso afin d'être <a href="https://txt.abrupt.cc/antilivre/placard_abrupt_heine_heinrich_tisserands_silesiens.pdf">lu, imprimé, placardé !</a> *Vorwärts!*

{{< figurehover src1="/img/190919-pauvres-tisserands-1.jpg" src2="/img/190919-pauvres-tisserands-2.jpg" alt="Pauvres tisserands" width="100%" >}}

&nbsp;<br>

<h1 class="glitch ligneblanche" data-text="Heinrich Heine">Heinrich Heine</h1>

<h1 class="glitch ligneblanche" data-text="Les tisserands tilésiens">Les tisserands silésiens</h1>

Dans l'œil sombre ne tombe aucun sanglot,<br>
Face à leur métier, ils montrent les crocs :<br>
Allemagne, nous tissons ton sindon,<br>
Y tissons la triple malédiction&nbsp;---<br>
Nous tissons, nous tissons&nbsp;!<br>

Malédiction sur le dieu que louèrent<br>
Nos prières dans la faim et l'hiver&nbsp;;<br>
En vain, nous avons attendu et cru,<br>
Il nous a moqués, dupés et perdus&nbsp;---<br>
Nous tissons, nous tissons&nbsp;!<br>

Malédiction sur le roi, roi des riches,<br>
Dur qui avec notre misère triche,<br>
Qui nous ravit jusqu'à nos derniers biens,<br>
Et nous fait abattre comme des chiens&nbsp;---<br>
Nous tissons, nous tissons&nbsp;!<br>

Malédiction sur la fausse patrie,<br>
Où seules croissent honte et infamie,<br>
Où chaque fleur si vite touche terre,<br>
Où l'ordure et l'infect gorgent le ver&nbsp;---<br>
Nous tissons, nous tissons&nbsp;!<br>

Le métier craque, la navette vole,<br>
Et jour et nuit, nous tissons sans paroles&nbsp;---<br>
Vieille Allemagne, nous tissons ton sindon,<br>
Y tissons la triple malédiction&nbsp;---<br>
Nous tissons, nous tissons&nbsp;!<br>

{{< figurelazy src="/img/190919-pauvres-tisserands-3.jpg" alt="Pauvres tisserands" width="100%" >}}

<h1 class="glitch ligneblanche" data-text="Die schlesischen Weber">Die schlesischen Weber</h1>

Im düstern Auge keine Thräne,<br>
Sie sitzen am Webstuhl und fletschen die Zähne:<br>
Deutschland, wir weben Dein Leichentuch,<br>
Wir weben hinein den dreifachen Fluch&nbsp;---<br>
Wir weben, wir weben!<br>

Ein Fluch dem Gotte, zu dem wir gebeten<br>
In Winterskälte und Hungersnöthen;<br>
Wir haben vergebens gehofft und geharrt,<br>
Er hat uns geäfft und gefoppt und genarrt&nbsp;---<br>
Wir weben, wir weben!<br>

Ein Fluch dem König, dem König der Reichen,<br>
Den unser Elend nicht konnte erweichen,<br>
Der den letzten Groschen von uns erpreßt,<br>
Und uns wie Hunde erschießen läßt&nbsp;---<br>
Wir weben, wir weben!<br>

Ein Fluch dem falschen Vaterlande,<br>
Wo nur gedeihen Schmach und Schande,<br>
Wo jede Blume früh geknickt,<br>
Wo Fäulniß und Moder den Wurm erquickt&nbsp;---<br>
Wir weben, wir weben!<br>

Das Schiffchen fliegt, der Webstuhl kracht,<br>
Wir weben emsig Tag und Nacht&nbsp;---<br>
Altdeutschland, wir weben Dein Leichentuch,<br>
Wir weben hinein den dreifachen Fluch,<br>
Wir weben, wir weben!<br>


