---
title: "Baudelaire hypertexte"
mot: "hypertexte"
date: "2018-10-08"
description: "Walter Benjamin dans <em>Das Passagen-Werk</em> qui cite Félix Tournachon, dit Nadar, qui cite donc Charles Baudelaire : « Symptômes de ruines. Bâtiments immenses, pélasgiens, l’un sur l’autre. Des appartements, des chambres, des temples, des galeries, des escaliers, des cœcums, des belvédères, des lanternes, des fontaines, des statues. — Fissures, lézardes. Humidité provenant d’un réservoir situé près du ciel. &mdash; Comment avertir les gens, les nations ? »"
images:
  - "img/181008-baudelaire.jpg"
sourceimage: "Spilliaert, Autoportrait / https://commons.wikimedia.org/wiki/File:L%C3%A9on_Spilliaert_(1908)_-_Zelfportret_met_rood_potlood.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "codex.cpp"
echo:
  - "Baudelaire"
  - "Benjamin"
  - "graphique"
  - "glitch"
---

\[...\] Walter Benjamin dans *Das Passagen-Werk* qui cite Félix Tournachon, dit Nadar, qui \[...\]

<div class="baffle-container">
<div class="baffle-container__box baffle-container__box-1">
Geplanter Gedichtzyklus »Onéirocritie«: »Symptômes de ruines. Bâtiments immenses, pélasgiens, l’un sur l’autre. Des appartements, des chambres, des temples, des galeries, des escaliers, des cœcums, des belvédères, des lanternes, des fontaines, des statues. &mdash; Fissures, lézardes. Humidité provenant d’un réservoir situé près du ciel. &mdash; Comment avertir les gens, les nations ? &mdash; Avertissons à l’oreille les plus intelligents. / Tout en haut, une colonne craque et ses deux extrémités se déplacent. Rien n’a encore croulé. Je ne peux retrouver l’issue. Je descends, puis je remonte. Une tour. &mdash; Labyrinthe. Je n’ai jamais pu sortir. J’habite pour toujours un bâtiment qui va crouler, un bâtiment travaillé par une maladie secrète. &mdash; Je calcule en moi-même, pour m’amuser, si une si prodigieuse masse de pierres, de marbres, de statues, de murs qui vont se choquer réciproquement, seront très-souillés par cette multitude de cervelles, de chairs humaines et d’ossements concassés. Je vois de si terribles choses en rêve, que je voudrais quelquefois ne plus dormir, si j’étais sûr de n’avoir pas trop de fatigue.« Nadar : Charles Baudelaire intime Paris 1911 p 136/37 [&lt;Baudelaire:Œuvres&gt; ed Le Dantec II p 696] 
</div>
</div>

{{< figurelazy src="/img/181008-glitchbaudelaire.gif" alt="Glitch de la photographie de Baudelaire par Nadar 1" width="50%" class="figure--centre" >}}

\[...\] cite donc Charles Baudelaire dans *Charles Baudelaire intime*, Paris 1911, chez A. Blaizot, éditeur, 26, rue Le Peletier, 26, qui \[...\]

<div class="baffle-container">
<div class="baffle-container__box baffle-container__box-2">
<p>La dernière pièce de mon dossier d’autographes : elle représente le premier jet d’un poème en prose qui, avec douze autres, &mdash; le savait-on ? &mdash; devait composer une suite sous le titre général : <em>Oneirocritée</em> ; mais il est bien difficile de n’y point trouver, comme dans presque tout ce qu’a écrit Baudelaire, l’accent de la confession autobiographique :</p>

<p>«&nbsp;Symptômes de ruines. Bâtiments immenses, Pélasgiens, l’un sur l’autre. Des appartements, des chambres, des <em>temples</em>, des galeries, des escaliers, des cæcums, des belvédères, des lanternes, des fontaines, des statues. &mdash; <em>Fissures, lézardes. Humidité provenant d’un réservoir situé près du ciel.</em> &mdash; Comment avertir les gens, les nations ? &mdash; Avertissons à l’oreille les plus intelligents.</p>

<p>Tout en haut une colonne craque et ses deux extrémités se déplacent. Rien n’a encore croulé. Je ne peux retrouver l’issue. Je descends, puis je remonte. <em>Une tour.</em> &mdash; <em>Labyrinthe. Je n’ai jamais pu sortir. J’habite pour toujours un bâtiment qui va crouler, un bâtiment travaillé par une maladie secrète.</em> &mdash; Je calcule en moi-même, pour m’amuser, si une si prodigieuse masse de pierres, de marbres, de statues, de murs qui vont se choquer réciproquement, seront très souillés par cette multitude de cervelles, de chairs humaines et d’ossements concassés. Je vois de si terribles choses en rêve, que je voudrais quelquefois ne plus dormir, si j’étais sûr de n’avoir pas trop de fatigue.&nbsp;»</p>

<p>Pauvre cher ! c’est lui-même qui est l’habitant du <em>bâtiment qui va crouler</em>, du bâtiment travaillé par un mal mystérieux ; bientôt, impuissant à les relever, il va contempler, jonchant le sol et brisées, les statues et les colonnes de tant de beaux poèmes qu’il portait encore en lui.</p>

<p>Je le revois aphasique et amaigri, mais toujours conscient et fidèle à son personnage.</p>
</div>
</div>

{{< figurelazy src="/img/181008-glitchbaudelaire-2.gif" alt="Glitch de la photographie de Baudelaire par Nadar 2" width="50%" class="figure--centre" >}}

\[...\] ne fait qu’ébaucher l’aphasie des pays pluvieux, son triste cerveau ou ce que le spleen n’a pu. \[...\] 

<div class="baffle-container">
<div class="baffle-container__box baffle-container__box-3">
<p>Symptômes de ruine. Bâtiments immenses. Plusieurs, l’un sur l’autre, des appartements, des chambres, <em>des temples</em>, des galeries, des escaliers, des cœcums, des belvédères, des lanternes, des fontaines, des statues. &mdash; <em>Fissures, lézardes. Humidité provenant d’un réservoir situé près du ciel.</em> &mdash; Comment avertir les gens, les nations &mdash; ? avertissons à l’oreille les plus intelligents.</p>

<p>Tout en haut, une colonne craque et ses deux extrémités se déplacent. Rien n’a encore croulé. Je ne peux plus retrouver l’issue. Je descends, puis je remonte. <em>Une tour-labyrinthe. Je n’ai jamais pu sortir. J’habite pour toujours un bâtiment qui va crouler, un bâtiment travaillé par une maladie secrète.</em> &mdash; Je calcule, en moi-même, pour m’amuser, si une si prodigieuse masse de pierres, de marbres, de statues, de murs, qui vont se choquer réciproquement seront très souillés par cette multitude de cervelles, de chairs humaines et d’ossements concassés. &mdash; Je vois de si terribles choses en rêve, que je voudrais quelquefois ne plus dormir, si j’étais sûr de n’avoir trop de fatigue.</p>
</div>
</div>

*\[...\] Nadar dans (le texte et puis dans) les glitchs \[...\]*

<script>
// Pour baffle
// Pour baffle dans symptômes de ruine
document.body.onload = function(){
  var s = document.querySelector('.baffle-container__box-1');
  var b = baffle(s, {
      characters: 'walterbenjamin',
      speed: 100
  }).start();
  var t = document.querySelector('.baffle-container__box-2');
  var c = baffle(t, {
      characters: 'felixtournachonnadar',
      speed: 100
  }).start();
  var u = document.querySelector('.baffle-container__box-3');
  var d = baffle(u, {
      characters: 'charlesbaudelaire',
      speed: 100
  }).start();

  s.addEventListener('mouseover', function() {
    b.reveal(2000);
  })
  t.addEventListener('mouseover', function() {
    c.reveal(2000);
  })
  u.addEventListener('mouseover', function() {
    d.reveal(2000);
  })

  s.addEventListener('mouseout', function() {
    b.start();
  })
  t.addEventListener('mouseout', function() {
    c.start();
  })
  u.addEventListener('mouseout', function() {
    d.start();
  })
};
</script>
