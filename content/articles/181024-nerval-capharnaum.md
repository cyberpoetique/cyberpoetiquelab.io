---
title: "Nerval en capharnaüm"
mot: "capharnaüm"
date: "2018-10-24"
description: "Des corridors, des corridors sans fin ! Des escaliers, des escaliers où l’on monte, où l’on descend, où l’on remonte, et dont le bas trempe toujours dans une eau noire agitée par des roues, sous d’immenses arches de pont… à travers des charpentes inextricables ! Monter, descendre, ou parcourir les corridors, et cela, pendant plusieurs éternités… Serait-ce la peine à laquelle je serais condamné pour mes fautes ? J’aimerais mieux vivre !"
images:
  - "img/181024-nerval.jpg"
sourceimage: "Soutine, Les maisons / https://commons.wikimedia.org/wiki/File:Soutine-2014-10.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "codex.cpp"
echo:
  - "Nerval"
  - "graphique"
---

Nos nuits d’octobre sont un *Capharnaüm*, la faute à Nerval (toujours) et à la rue de la Vieille-Lanterne (un peu).

<div class="resize-container ligneblanche">

<span class="resize-drag">Des corridors,</span><span class="resize-drag">des corridors</span><span class="resize-drag">sans fin&nbsp;!</span><span class="resize-drag">Des escaliers,</span><span class="resize-drag">des escaliers</span><span class="resize-drag">où l’on monte,</span><span class="resize-drag">où l’on descend,</span><span class="resize-drag">où l’on remonte,</span><span class="resize-drag">et dont le bas trempe</span><span class="resize-drag">toujours dans une eau noire</span><span class="resize-drag">agitée par des roues,</span><span class="resize-drag">sous d’immenses arches</span><span class="resize-drag">de pont…</span><span class="resize-drag">à travers</span><span class="resize-drag">des charpentes</span><span class="resize-drag">inextricables&nbsp;!</span><span class="resize-drag">Monter,</span><span class="resize-drag">descendre,</span><span class="resize-drag">ou parcourir</span><span class="resize-drag">les corridors,</span><span class="resize-drag">et cela,</span><span class="resize-drag">pendant plusieurs éternités…</span><span class="resize-drag">Serait-ce</span><span class="resize-drag">la peine</span><span class="resize-drag">à laquelle</span><span class="resize-drag">je serais</span><span class="resize-drag">condamné</span><span class="resize-drag">pour mes fautes&nbsp;?</span><span class="resize-drag">J’aimerais</span><span class="resize-drag">mieux vivre&nbsp;!</span></span><span class="resize-drag">Au contraire,</span><span class="resize-drag">voilà</span><span class="resize-drag">qu’on me brise</span><span class="resize-drag">la tête</span><span class="resize-drag">à grands coups</span><span class="resize-drag">de marteau&nbsp;:</span><span class="resize-drag">qu’est-ce</span><span class="resize-drag">que cela</span><span class="resize-drag">veut dire&nbsp;?</span></span><span class="resize-drag">Je rêvais</span><span class="resize-drag">à des queues</span><span class="resize-drag">de billard…</span><span class="resize-drag">à des petits verres</span><span class="resize-drag">de verjus…</span></span><span class="resize-drag">«&nbsp;Monsieur</span><span class="resize-drag">et mame</span><span class="resize-drag">le maire</span><span class="resize-drag">est-il</span><span class="resize-drag">content&nbsp;?&nbsp;»</span><span class="resize-drag">Bon&nbsp;!</span><span class="resize-drag">je confonds</span><span class="resize-drag">à présent</span><span class="resize-drag">Bilboquet</span><span class="resize-drag">avec Macaire.</span><span class="resize-drag">Mais</span><span class="resize-drag">ce n’est pas</span><span class="resize-drag">une raison</span><span class="resize-drag">pour qu’on me casse</span><span class="resize-drag">la tête</span><span class="resize-drag">avec des foulons.</span><span class="resize-drag">«&nbsp;Brûler</span><span class="resize-drag">n’est pas</span><span class="resize-drag">répondre&nbsp;!&nbsp;»</span><span class="resize-drag">Serait-ce</span><span class="resize-drag">pour avoir embrassé</span><span class="resize-drag">la femme</span><span class="resize-drag">à cornes,</span><span class="resize-drag">ou pour avoir</span><span class="resize-drag">promené</span><span class="resize-drag">mes doigts</span><span class="resize-drag">dans sa chevelure</span><span class="resize-drag">de mérinos&nbsp;?</span><span class="resize-drag">«&nbsp;Qu’est-ce</span><span class="resize-drag">que c’est donc</span><span class="resize-drag">que ce cynisme&nbsp;!&nbsp;»</span><span class="resize-drag">dirait Macaire.</span><span class="resize-drag">Mais</span><span class="resize-drag">Desbarreaux</span><span class="resize-drag">le cartésien</span><span class="resize-drag">répondrait</span><span class="resize-drag">à la Providence&nbsp;:</span><span class="resize-drag">«&nbsp;Voilà</span><span class="resize-drag">bien</span><span class="resize-drag">du tapage</span><span class="resize-drag">pour…</span><span class="resize-drag">bien peu</span><span class="resize-drag">de chose.&nbsp;»</span>

</div>

Le texte figure dans le chapitre XVII des *Nuits d’octobre* de Gérard de Nerval.

<!-- Capharnaüm

Des corridors, des corridors sans fin&nbsp;! Des escaliers, des escaliers où l’on monte, où l’on descend, où l’on remonte, et dont le bas trempe toujours dans une eau noire agitée par des roues, sous d’immenses arches de pont… à travers des charpentes inextricables&nbsp;! Monter, descendre, ou parcourir les corridors, et cela, pendant plusieurs éternités… Serait-ce la peine à laquelle je serais condamné pour mes fautes&nbsp;?

J’aimerais mieux vivre&nbsp;!

Au contraire, voilà qu’on me brise la tête à grands coups de marteau&nbsp;: qu’est-ce que cela veut dire&nbsp;?

Je rêvais à des queues de billard… à des petits verres de verjus…

«&nbsp;Monsieur et mame le maire est-il content&nbsp;?&nbsp;»

Bon&nbsp;! je confonds à présent Bilboquet avec Macaire. Mais ce n’est pas une raison pour qu’on me casse la tête avec des foulons.

«&nbsp;Brûler n’est pas répondre&nbsp;!&nbsp;»

Serait-ce pour avoir embrassé la femme à cornes, ou pour avoir promené mes doigts dans sa chevelure de mérinos&nbsp;?

«&nbsp;Qu’est-ce que c’est donc que ce cynisme&nbsp;!&nbsp;» dirait Macaire.

Mais Desbarreaux le cartésien répondrait à la Providence&nbsp;:

«&nbsp;Voilà bien du tapage pour… bien peu de chose.&nbsp;»-->

*À la faute à la photo à Nadar (aussi)*

<script src="/js/interact.min.js"></script>
<script>
  /* enable javascript to view a demo */
  interact('.resize-drag')
  .draggable({
    onstart: function (event) {
      event.target.style.zIndex = parseInt(new Date().getTime() / 1000);
    },
    onmove: window.dragMoveListener,
    restrict: {
      restriction: 'parent',
      elementRect: { top: 1, left: 1, bottom: 1, right: 1 }
    },
  })
  .resizable({
    onstart: function (event) {
      event.target.style.zIndex = parseInt(new Date().getTime() / 1000);
    },
    // resize from all edges and corners
    edges: { left: true, right: true, bottom: true, top: true },
    // keep the edges inside the parent
    restrictEdges: {
      outer: 'parent',
      endOnly: true,
    },
    // minimum size
    restrictSize: {
      min: { width: 50, height: 50 },
      //restriction: 'parent',
      //endOnly: true,
    },
    inertia: true,
  })
  .on('resizemove', function (event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);

    // update the element's style
    target.style.width  = event.rect.width + 'px';
    target.style.height = event.rect.height + 'px';
    // translate when resizing from top or left edges
    x += event.deltaRect.left;
    y += event.deltaRect.top;

    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)';
   target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    //target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height);
  });

  /* enable javascript to view a demo */
  // target elements with the "draggable" class
  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;
</script>
