---
title: "Ne travaillez jamais"
slug: "ne travaillez jamais"
mot: "travail"
date: "2020-05-01"
description: "Bagnarde intraitable sur qui se referme toujours l'enfer. Nous t'admirons. Nous visitons les auberges et les garnis que tu aurais sacrés par ton séjour. Nous voyons avec ton idée le ciel pourpre et le travail fleuri du pré. Ta fatalité dans les villes. Nous la flairons. Tu as plus de force qu'une sainte. Plus de bon sens que la fortune --- oh notre harpie, nos amours clandestines. Et toi, toi seule, pour témoin de ta gloire et de ta raison."
images:
  - "img/200501-ne-travaillez-jamais.jpg"
sourceimage: "Edvard Munch, Workers on their Way Home / https://commons.wikimedia.org/wiki/File:Edvard_Munch_-_Workers_on_their_Way_Home_-_Google_Art_Project.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "fabrica.f"
echo:
  - "Rimbaud"
  - "placard"
  - "collage"
  - "travail"
  - "queer"
  - "glitch"
  - "situations"
---

*Ne travaillez jamais. Tout n'est que vol. Nous sommes en paix.*

*Ce placard (ouvrage d'escarpe, Harar en souvenir) se place dans le domaine public volontaire, sous la licence Creative Commons Zero, comme le précise notre politique de* [partage](https://abrupt.cc/partage). *Il est disponible dans un format A3 recto-verso afin d'être* <a href="https://txt.abrupt.cc/antilivre/placard_abrupt_la_cyberpoetique_ne_travaillez_jamais.pdf">lu, imprimé, placardé.</a>

{{< figurelazy src="/img/200501-ne-travaillez-jamais-1.jpg" alt="Ne travaillez jamais" width="100%" >}}

<h1 class="ligneblanche centre"><span class="glitch" data-text="Ne travaillez jamais">Ne travaillez jamais</span></h1>

Bagnarde intraitable sur qui se referme toujours l'enfer. Nous t'admirons.

Nous visitons les auberges et les garnis que tu aurais sacrés par ton séjour. Nous voyons avec ton idée le ciel pourpre et le travail fleuri du pré. Ta fatalité dans les villes. Nous la flairons. Tu as plus de force qu'une sainte. Plus de bon sens que la fortune --- oh notre harpie, nos amours clandestines. Et toi, toi seule, pour témoin de ta gloire et de ta raison.

Mais les blancs débarquent. Le canon !

Il faut se soumettre au baptême, s'habiller, travailler.

Quant au bonheur établi, domestique ou non... non, nous ne pouvons pas. Nous sommes trop dissipées, trop faibles. La vie bourgeonne par le travail. Vieille vérité : nous, notre vie n'est pas assez pesante, elle s'envole et flotte loin au-dessus de l'action, ce cher point du monde.

Fiez-vous donc à nous. La foi soulage, guide, guérit. Toutes, venez, --- même les petites enfants, --- que nous vous consolions, qu'on répande pour vous notre cœur, --- le cœur merveilleux ! --- Pauvres femmes, travailleuses ! Nous ne demandons pas de prières ; avec votre confiance seulement, nous serons heureuses.

Vous nous écoutez faisant de l'infamie une gloire, de la cruauté un charme. Nous sommes une sous-race. La race lointaine de nos mères. Elles étaient la glaise et le feu. Elles se perçaient les côtes, buvaient leur sang. Nous nous ferons des entailles partout le corps. Nous nous tatouerons.

{{< figurelazy src="/img/200501-ne-travaillez-jamais-2.jpg" alt="Ne travaillez jamais" width="100%" >}}

Nous voulons devenir hideuses comme les vestales. Nous hurlerons dans les rues. Vous verrez. Nous voulons devenir bien folles de rage.

Ne nous montrez jamais de bijoux, nous ramperions et nous tordrions sur le tapis. Notre richesse, nous la voudrions tachée de sang partout. Jamais nous ne travaillerons.

Nous nous voyons comme de bonnes enfants. Libres de se promener dans le Paradis de tristesse. Accordons-nous. Et bien émues affirmons : nous travaillerons ensemble à ne jamais travailler.

Ah ! nous n'avons jamais été jalouses de vous. Que devenir ? Nous n'avons pas une connaissance ; nous ne travaillerons jamais. Nous voulons vivre somnambules. Seules, notre bonté et notre charité nous donnent droit dans le monde réel.

Par instants, nous oublions la pitié où nous sommes tombées. Du rêve et des révoltes.

Nous nous rendrons fortes. Nous voyagerons, nous chasserons dans les déserts, nous dormirons sur les pavés des villes inconnues, sans soins, sans peines.

Ou nous nous réveillerons, et les lois et les mœurs auront changé. Grâce à notre vertu sorcière. Mais le monde, en restant le même, nous laisse à nos désirs, joies, nonchalances.

Ô Furies des forges, portez aux travailleuses l'eau-de-vie. Que leurs forces soient en paix. En attendant la mer, la cloche et l'ivresse des midis.

Le travail humain : c'est l'explosion qui éclaire notre abîme.

{{< figurelazy src="/img/200501-ne-travaillez-jamais-3.jpg" alt="Ne travaillez jamais" width="100%" >}}


Rien n'est vanité. En avant ! Et pourtant les cadavres des méchants et des fainéants tombent sur le cœur des autres. Vite, vite un peu. Là-bas, par delà la nuit. Nos récompenses futures, éternelles. Notre échappée.

Qu'y pouvons-nous ? Nous connaissons le travail. Et la science est trop lente. Que la prière galope et que la lumière gronde nous le voyons bien. C'est trop simple, et il fait trop chaud. On se passera de nous. Nous avons notre devoir. Nous en serons fières à la façon des peuples.

Non ! non ! à présent nous nous révoltons contre votre mort ! Le travail paraît trop léger à notre orgueil. Notre trahison au monde serait un supplice trop court. Au dernier moment, nous attaquerions à droite, à gauche.

Alors, --- oh ! --- chères pauvres âmes, l'éternité serait-elle une lame entre nos mains ?

Du même désert, à la même nuit. Toujours nos yeux las se réveillent à l'étoile d'argent, toujours, sans que s'émeuvent les Reines impunies, les trois sœurs. Mercure, sel et soufre ; notre évasion est impure. Quand irons-nous par delà les grèves et les monts ? Saluer la naissance du travail nouveau, l'assassiner. Notre absolution. L'ivresse : sagesse nouvelle. La fuite des tyrans et des démons. La fin de la superstition. Adorer --- nous les premières ! --- Saturne sous la terre. Nos langues, creusets d'émeutes. Y goutte l'orage.

Le chant des cieux, notre marche des peuples. Esclaves, ne maudissons pas la vie.


