---
title: "Seol Seol"
mot: "incendie"
date: "2020-07-16"
images:
  - "img/200716-seol-seol.jpg"
sourceimage: "August Macke, couple dans la forêt / https://commons.wikimedia.org/wiki/File:August_Macke_-_Couple_on_the_Forest_Track_-_Google_Art_Project.jpg"
persona:
  - "Étienne Michelet"
espace:
  - "derivo.d"
echo:
  - "graphique"
  - "littérature"
  - "glitch"
library:
  - "vanilla-tilt.min.js"
script: |
  // Musique
  var audio = document.getElementById("fond-sonore");
  // var musiqueJoue = "pause";
  function fondSonore() {
      if (audio.paused) {
          audio.play();
      }
      else {
          audio.pause();
      }
  };
  const musique = document.querySelector('.music');
  musique.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector('.music--on').classList.toggle('hidden');
    document.querySelector('.music--off').classList.toggle('hidden');
    fondSonore();
  });

  document.querySelectorAll(".tilt").forEach((el) => {
    el.addEventListener("mouseenter", function(event) {   
      event.target.style.opacity = 0.6;
    });
    el.addEventListener("mouseleave", function(event) {   
      event.target.style.opacity = 0.8;
    });
  });

  VanillaTilt.init(document.querySelectorAll(".tilt"), {
    perspective: 700,
    speed: 100,
    glare: true,
    "max-glare": 0.7,
   gyroscopeMinAngleX: -5,
    gyroscopeMaxAngleX: 5, 
    gyroscopeMinAngleY: -5,
    gyroscopeMaxAngleY: 5,
  });

  document.querySelectorAll(".highlight").forEach((el) => {
    el.addEventListener("click", function(event) {   
      event.target.classList.toggle('highlight--invisible');
    });
  });
---

<audio loop id="fond-sonore">
<source src="/img/200716-seol-seol.mp3" type="audio/mpeg">
</audio>

<br>

<p style="text-align: center;">
<button class="btn btn--music music">
  <span class="music--on hidden" >MUSIQUE</span>
  <span class="music--off">MUSIQUE</span>
</button>
</p>

<br>

{{< figureglitch src="/img/200716-seol-seol-incendie.jpg" alt="Seol Seol" width="70%" margin="0 auto" class="tilt" div-style="position: -webkit-sticky; position: sticky; top: 2em; opacity: 0.8;" >}}

<br>

<div style="display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center;">
<div style="display: inline-block; z-index: 50;">
<span class="highlight highlight--red highlight--invisible">la plante en rhizomes</span><br>
<span class="highlight highlight--red highlight--invisible">sans fleurs ni graines</span><br>
<span class="highlight highlight--red highlight--invisible">et tes yeux parmi les bambous</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">sais-tu qu'aucun cercle</span><br>
<span class="highlight highlight--red highlight--invisible">ne se brise ?</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens</span><br>
<span class="highlight highlight--red highlight--invisible">se sont mis à tourner</span><br>
<span class="highlight highlight--red highlight--invisible">à mordre</span><br>
<span class="highlight highlight--red highlight--invisible">la chienne seule</span><br>
<span class="highlight highlight--red highlight--invisible">sous le ciel blanc</span><br>
<span class="highlight highlight--red highlight--invisible">la lune ne bougeait pas</span><br>
<span class="highlight highlight--red highlight--invisible">et ton œil ton visage</span><br>
<span class="highlight highlight--red highlight--invisible">ne bougeaient pas</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">toi tu n'aurais même pas</span><br>
<span class="highlight highlight--red highlight--invisible">écrasé avec tes doigts</span><br>
<span class="highlight highlight--red highlight--invisible">l'insecte sur ta peau</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">toi tu ne bougeais pas</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens tournaient</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens mordaient</span><br>
<span class="highlight highlight--red highlight--invisible">la chienne seule</span><br>
<span class="highlight highlight--red highlight--invisible">sous le ciel blanc</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">la langue dans la bouche</span><br>
<span class="highlight highlight--red highlight--invisible">il fallait la tourner mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">avant de tout brûler</span><br>
<span class="highlight highlight--red highlight--invisible">et la retourner encore mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">avant d'allumer l'incendie</span><br>
<span class="highlight highlight--red highlight--invisible">ou encore regarder mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">les oiseaux qui remuent</span><br>
<span class="highlight highlight--red highlight--invisible">comme des automates</span><br>
<span class="highlight highlight--red highlight--invisible">dans les branches souples</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">regarde le chien qui fixe</span><br>
<span class="highlight highlight--red highlight--invisible">un certain temps les oiseaux</span><br>
<span class="highlight highlight--red highlight--invisible">ou regarde encore</span><br>
<span class="highlight highlight--red highlight--invisible">le chien qui écoute</span><br>
<span class="highlight highlight--red highlight--invisible">un certain temps</span><br>
<span class="highlight highlight--red highlight--invisible">les pleurs du nourrisson</span><br>
<span class="highlight highlight--red highlight--invisible">les reptiles méritent tes caresses</span><br>
<span class="highlight highlight--red highlight--invisible">comme on caresse</span><br>
<span class="highlight highlight--red highlight--invisible">comme on effleure les cordes</span><br>
<span class="highlight highlight--red highlight--invisible">d'un instrument à cordes</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">tes cordes vocales</span><br>
<span class="highlight highlight--red highlight--invisible">sont de la magie pour le monde</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">ça te fait sourire</span><br>
<span class="highlight highlight--red highlight--invisible">toutes ces flammes en l'air</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes levées</span><br>
<span class="highlight highlight--red highlight--invisible">comme des langues</span><br>
<span class="highlight highlight--red highlight--invisible">dans le ciel blanc</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes dans tes yeux</span><br>
<span class="highlight highlight--red highlight--invisible">ça te fait sourire</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">de voir le monde brûler</span><br>
<span class="highlight highlight--red highlight--invisible">dans tes yeux</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">les flammes dansent</span><br>
<span class="highlight highlight--red highlight--invisible">dans le champ de canne à sucre</span><br>
<span class="highlight highlight--red highlight--invisible">toi tu danses sur le chemin</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens aboient</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes dans tes yeux</span><br>
<span class="highlight highlight--red highlight--invisible">et dans ton sourire le monde brûle</span><br>
<span class="highlight highlight--red highlight--invisible">et dans ta danse le monde brûle</span><br>
<br>
<br>
<br>
</div>
</div>
