---
title: "HTTP la hess"
mot: "hess"
date: "2018-12-25"
description: "à son errance elle vomit de la silice les intelligences les artificielles ses sœurs et ses frères pullulent entre ses organes elle porte sa carcasse sous le ou les réseaux et vibre vibrance voyance elle s’arrache l’œil de la voyance le jette à l’anonyme des groupes dissolution des groupes acides s’écoulent les seringues la maintiennent s’enfoncent et ce qui lui reste d’humain ce qui veut s’échapper s’évapore on trafique l’identité elle trafique l’identité la clandestine et faire la nique à la rue à la surveillance des rues parce que la piraterie est une femme qui transe transmet son futur"
images:
  - "img/181225-http-la-hess.jpg"
sourceimage: "Spilliaert, De zeemeerminnen / https://commons.wikimedia.org/wiki/File:L%C3%A9on_Spilliaert_(1908)_-_De_zeemeerminnen.jpg"
persona:
  - "Ann Persson"
espace:
  - "derivo.d"
echo:
  - "graphique"
  - "rêve"
---

*(survole et surligne et rêve ou révélation)*

<p class="ivresse shake--continu">à son errance elle vomit de la silice les intelligences les artificielles ses sœurs et ses frères pullulent entre ses organes elle porte sa carcasse sous le ou les réseaux et vibre vibrance voyance elle s’arrache l’œil de la voyance le jette à l’anonyme des groupes dissolution des groupes acides s’écoulent les seringues la maintiennent s’enfoncent et ce qui lui reste d’humain ce qui veut s’échapper s’évapore on trafique l’identité elle trafique l’identité la clandestine et faire la nique à la rue à la surveillance des rues parce que la piraterie est une femme qui transe transmet son futur et sa voix qui dévoile le futur des pirateries la piraterie la féministe n’est jamais finie court-circuit sur course sous bière sourd de biture le marché les marchés qui trafiquent le réel et le réel qui se trafique contre ou contre marché se venge s’écoule l’irréel elle branche la pirate se branche sur protocole l’ancien HTTP la hess ça s’évapore avec son identité prisme sonde son identité qu’elle se divise à sa division elle frappe le marteau chante que dit-il dit-elle le marteau ou la pirate c’est elle qui l’émascule elle se passe de marteau elle ne l’entend le casse sous crâne la martre marteau qu’elle emporte amoureuse à son ombre l’amour d’ombre martre à sa métamorphose martre en chacal elle sort l’arme antique les douze coups contre monde contre piloris l’immonde elle pointe le canon fractal sur l’identité la nationale crache sur et frappe et tire s’extirpe de tempe l’identité se multiplie pullule d’elle et sur tempe et tire à blanc douze coups et recharge et frappe et règne le marteau s’étiole dans les acides et se multiplient les identités les genres contre viol la violence fol le folliculaire qui branche son clavier sur contre contre-courant s’électrisent les ossements ils tintent les ossements ils hurlent les chamanes la chamane qui danse transe des viols violences ou vol et la voleuse rêve de la pirate qui remonte les gouffres rapièce les secondes opère les chacals la pirate elle est un chacal qui parcourt les transes les réseaux déterre détruit les ruines les enfouit dans sa seringue transe sans trêve rêve s’excave le ou les réseaux la pirate rêve</p>
