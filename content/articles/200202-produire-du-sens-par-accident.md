---
title: "Produire du sens par accident"
mot: "accident"
date: "2020-02-02"
description: "Anthropie a l'irresponsabilité séditieuse de laisser Dio jouer avec le hasard, infecter les marchés, travestir les récits et révéler la perte du sens. Dio a peut-être l'arrogance des maîtres, mais Dio détourne leur pouvoir pour oser dire..."
images:
  - "img/200202-produire-du-sens-par-accident.jpg"
sourceimage: "El Lissitzky, Proun. 1st Kestner Portfolio / https://en.wikipedia.org/wiki/File:El_Lissitzky_-_1o_Kestnermappe_Proun_(Proun._1st_Kestner_Portfolio)_-_Google_Art_Project.jpg"
persona:
  - "Anthropie"
espace:
  - "codex.cpp"
echo:
  - "graphique"
  - "hasard"
library:
  - "baffle.min.js"
script: |
  function shuffle(string, space = ' ') {
    let stringBreak = string.split("");
    let stringBreakLength = stringBreak.length;
    let entropy = stringBreakLength - (Math.floor(Math.random() * 7) + 1);
  
    for (let i = stringBreakLength - 1; i > entropy; i--) {
      if (stringBreak[i] != space) {
        let j = Math.floor(Math.random() * (i + 1));
        if (stringBreak[j] != space) {
          let stringTemp = stringBreak[i];
          stringBreak[i] = stringBreak[j];
          stringBreak[j] = stringTemp;
        }
      }
    }
    return stringBreak.join("");
  }

  let hamlet = baffle('.baffle--hamlet', {
    characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    speed: 200
  });

  document.querySelector('.baffle-container__box').addEventListener('mouseover', function() {
    hamlet.start();
  });

  document.querySelector('.baffle-container__box').addEventListener('mouseout', function() {
    hamlet.stop();
  });

  document.querySelector('.btn--submit').addEventListener('click', (e) => {
    e.preventDefault();
    let par = document.createElement('p');
    par.classList.add('nomargin');
    let phrase = document.createTextNode(shuffle("produisant du sens par accident"));
    par.appendChild(phrase);
    document.querySelector('.produire').prepend(par);
  });
---

*Anthropie a l'irresponsabilité séditieuse de laisser [Dio](https://abrupt.cc/anthropie/dio) jouer avec le hasard, infecter les marchés, travestir les récits et révéler la perte du sens. Dio a peut-être l'arrogance des maîtres, mais Dio détourne leur pouvoir pour oser dire...*

même les intelligences artificielles<br>
les plus idiotes<br>
se moquaient de toi<br>

tu te disais que la signification<br>
était un concept relatif<br>
que toute production de sens<br>
était accidentelle<br>
un accident qui arrivait mille fois par jour<br>
à sept milliards d’êtres humains<br>

c’est sans doute<br>
la découverte scientifique<br>
la plus importante de ton ère :<br>
un singe à qui l’on donnerait<br>
une machine à écrire<br>
et une infinité de temps<br>
finirait nécessairement<br>
par écrire un chef-d’œuvre<br>
de la littérature mondiale<br>


<table class="baffle-container__box">
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">H</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">26</span></td></tr>
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">A</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">676</span></td></tr>
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">M</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">17 576</span></td></tr>
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">L</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">456 976</span></td></tr>
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">E</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">11 881 376</span></td></tr>
<tr><td style = "width: 3em; overflow: hidden; vertical-align: top;">écrire</td><td style = "width: 1.2em; overflow: hidden; vertical-align: top;"><span class="baffle--hamlet">T</span></td><td style = "vertical-align: top">:&nbsp;</td><td style = "vertical-align: top">une chance sur <span style="display: inline-block; width: 6em;">308 915 776</span></td></tr>
</table>

l’être humain·e n’était<br>
qu’un générateur de texte<br><button class="btn--submit shake shuffle" type="submit">aléatoire</button>

<div class="produire">
produisant du sens par accident<br>
produiasnt du sens par accidnet<br>
prodiuasnt du snes pra accidnet<br>
pordiusant ud ness rap cicadent<br>
ourpidsant du snes arp iccdaent<br>
pourdisnat du sens pra daccient<br>
poruidsant du enss arp adccient<br>
produisant du sens rap acidentc<br>
produisant du sens par accident<br>
</div>
