﻿---
title: "De l’archéotexte"
mot: "archéotexte"
date: "2018-09-17"
description: "Prendre un texte, original ou non, le transformer en une version HTML, et faire en sorte qu’à l’aide d’un peu de Javascript et d’une API d’un dictionnaire, celle du wiktionary par exemple, chaque mot, aussi insignifiant soit-il, propose lors de son survol son étymologie, une étymologie qui remonterait jusqu’aux origines de la langue, jusqu’aux traces de l’indo-européen."
images:
  - "img/180917-archeotextes.jpg"
sourceimage: "Kliun, L'horloger / https://commons.wikimedia.org/wiki/File:Ivan_Vasilievich_Kliun_-_The_Clockmaker_(Der_Uhrmacher-_L%27horloger)_-_2007.85_-_Minneapolis_Institute_of_Arts.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "scriptorium.sh"
echo:
  - "exhortation"
  - "code"
---

Prendre un texte, original ou non, le transformer en une version HTML, et faire en sorte qu’à l’aide d’un peu de JavaScript et d’une API d’un dictionnaire, <a href="https://en.wiktionary.org/w/api.php" target="_blank">celle du wiktionary par exemple</a>, chaque mot, aussi insignifiant soit-il, propose lors de son survol son étymologie, une étymologie qui remonterait jusqu’aux origines de la langue, jusqu’aux traces de l’indo-européen. Et imaginer même, avec un simple bouton, faire disparaître chaque mot et laisser à la place la seule étymologie sans aucune majuscule ni aucun point. Un texte comme une continuité des traces, l’enchevêtrement de fossiles langagiers pour une archéologie faite poésie.

(Cette idée est une invitation.)

