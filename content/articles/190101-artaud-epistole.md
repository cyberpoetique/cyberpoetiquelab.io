---
title: "Artaud épistole"
mot: "épistole"
date: "2019-01-01"
description: "Monsieur le législateur de la loi de 1916, agrémentée du décret de juillet 1917 sur les stupéfiants, tu es un con. Ta loi ne sert qu’à embêter la pharmacie mondiale sans profit pour l’étiage toxicomanique de la nation parce que 1° Le nombre des toxicomanes qui s’approvisionnent chez le pharmacien est infime ; 2° Les vrais toxicomanes ne s’approvisionnent pas chez le pharmacien ; 3° Les toxicomanes qui s’approvisionnent chez le pharmacien sont tous des malades ; 4° Le nombre des toxicomanes malades est infime par rapport à celui des toxicomanes voluptueux ; 5° Les restrictions pharmaceutiques de la drogue ne gêneront jamais les toxicomanes voluptueux et organisés ; 6° Il y aura toujours des fraudeurs ; 7° Il y aura toujours des toxicomanes par vice de forme, par passion ; 8° Les toxicomanes malades ont sur la société un droit imprescriptible, qui est celui qu’on leur foute la paix. C’est avant tout une question de conscience."
images:
  - "img/190101-artaud-epistole.jpg"
sourceimage: "Klee, Gespent eines Genies / https://commons.wikimedia.org/wiki/File:Paul_Klee_-_Gespenst_eines_Genies_(Ghost_of_a_Genius)_-_Google_Art_Project.jpg"
persona:
  - "La Cyberpoétique"
espace:
  - "fabrica.f"
echo:
  - "microantilivre"
  - "Artaud"
  - "glitch"
---

*Avec Antonin Artaud, sa Lettre à Monsieur le législateur, sa lettre à tous les législateurs, à toutes les époques, les limbes s’étirent, recouvrent l’horizon, et l’humain découvre sa division, son esprit qui s’ouvre à la vie tout entière.* 

*Ce* [microantilivre](https://www.antilivre.org/#microantilivre) *est disponible dans sa* <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_artaud_antonin_epistole.pdf">version PDF</a> *ainsi que dans sa* <a href="https://txt.abrupt.cc/antilivre/microantilivre_abrupt_artaud_antonin_epistole_imprimable.pdf">version imprimable DIY.</a>

{{< figurelazy src="/img/190101-artaud-glitch.gif" alt="Glitch de la photographie de Antonin Artaud" width="50%" class="figure--centre" >}}


<h1 class="ligneblanche centre"><span class="glitch" data-text="Lettre à Monsieur le législateur de la loi sur les stupéfiants">Lettre à Monsieur le législateur de la loi sur les stupéfiants</h1>
<h2 class="centre ligneblanche"><span class="glitch" data-text="Antonin Artaud">Antonin Artaud</h2>

Monsieur le législateur,

Monsieur le législateur de la loi de 1916, agrémentée du décret de juillet 1917 sur les stupéfiants, tu es un con.

Ta loi ne sert qu’à embêter la pharmacie mondiale sans profit pour l’étiage toxicomanique de la nation

parce que

1° Le nombre des toxicomanes qui s’approvisionnent chez le pharmacien est infime&nbsp;;

2° Les vrais toxicomanes ne s’approvisionnent pas chez le pharmacien&nbsp;;

3° Les toxicomanes qui s’approvisionnent chez le pharmacien sont *tous* des malades&nbsp;;

4° Le nombre des toxicomanes malades est infime par rapport à celui des toxicomanes voluptueux&nbsp;;

5° Les restrictions pharmaceutiques de la drogue ne gêneront jamais les toxicomanes voluptueux et organisés&nbsp;;

6° Il y aura toujours des fraudeurs&nbsp;;

7° Il y aura toujours des toxicomanes par vice de forme, par passion&nbsp;;

8° Les toxicomanes malades ont sur la société un droit imprescriptible, qui est celui qu’on leur foute la paix.

C’est avant tout une question de conscience.

La loi sur les stupéfiants met entre les mains de l’inspecteur-usurpateur de la santé publique le droit de disposer de la douleur des hommes&nbsp;; c’est une prétention singulière de la médecine moderne que de vouloir dicter ses devoirs à la conscience de chacun. Tous les bêlements de la charte officielle sont sans pouvoir d’action contre ce fait de conscience : à savoir, que, plus encore que de la mort, je suis le maître de ma douleur. Tout homme est juge, et juge exclusif, de la quantité de douleur physique, ou encore de vacuité mentale qu’il peut honnêtement supporter.

Lucidité ou non-lucidité, il y a une lucidité que nulle maladie ne m’enlèvera jamais, c’est celle qui me dicte le sentiment de ma vie physique[^1]. Et si j’ai perdu ma lucidité, la médecine n’a qu’une chose à faire, c’est de me donner les substances qui me permettent de recouvrer l’usage de cette lucidité.

Messieurs les dictateurs de l’école pharmaceutique de France, vous êtes des cuistres rognés: il y a une chose que vous devriez mieux mesurer&nbsp;; c’est que l’opium est cette imprescriptible et impérieuse substance qui permet de rentrer dans la vie de leur âme à ceux qui ont eu le malheur de l’avoir perdue.

Il y a un mal contre lequel l’opium est souverain et ce mal s’appelle l’Angoisse, dans sa forme mentale, médicale, physiologique, logique ou pharmaceutique, comme vous voudrez.

L’Angoisse qui fait les fous.

L’Angoisse qui fait les suicidés.

L’Angoisse qui fait les damnés.

L’Angoisse que la médecine ne connaît pas.

L’Angoisse que votre docteur n’entend pas.

L’Angoisse qui lèse la vie.

L’Angoisse qui pince la corde ombilicale de la vie.

{{< figurelazy src="/img/190101-artaud-portrait.jpg" alt="Photographie de Antonin Artaud" width="50%" class="figure--centre" >}}

Par votre loi inique vous mettez entre les mains de gens en qui je n’ai aucune espèce de confiance, cons en médecine, pharmaciens en fumier, juges en mal-façon, docteurs, sages-femmes, inspecteurs-doctoraux, le droit de disposer de mon angoisse, d’une angoisse en moi aussi fine que les aiguilles de toutes les boussoles de l’enfer.

Tremblements du corps ou de l’âme, il n’existe pas de sismographe humain qui permette à qui me regarde d’arriver à une évaluation de ma douleur plus précise, que celle, foudroyante, de mon esprit&nbsp;!

Toute la science hasardeuse des hommes n’est pas supérieure à la connaissance immédiate que je puis avoir de mon être. Je suis seul juge de ce qui est en moi.

Rentrez dans vos greniers, médicales punaises, et toi aussi, Monsieur le Législateur Moutonnier, ce n’est pas par amour des hommes que tu délires, c’est par tradition d’imbécillité. Ton ignorance de ce que c’est qu’un homme n’a d’égale que ta sottise à le limiter. Je te souhaite que ta loi retombe sur ton père, ta mère, ta femme, tes enfants, et toute ta postérité. Et maintenant avale ta loi.

[^1]: Je sais assez qu’il existe des troubles graves de la personnalité, et qui peuvent même aller pour la conscience jusqu’à la perte de son individualité : la conscience demeure intacte mais ne se reconnaît plus comme s’appartenant (et ne se reconnaît plus à aucun degré).

    Il y a des troubles moins graves, ou pour mieux dire moins essentiels, mais beaucoup plus douloureux et plus importants pour la personne, et en quelque sorte plus *ruineux* pour la vitalité, c’est quand la conscience s’approprie, reconnaît vraiment comme lui appartenant toute une série de phénomènes de dislocation et de dissolution de ses forces au milieu desquels sa matérialité se détruit.

    Et c’est à ceux-là même que je fais allusion.

    Mais il s’agit justement de savoir si la vie n’est pas plus atteinte par une décorporisation de la pensée avec conservation d’une parcelle de conscience, que par la projection de cette conscience dans un indéfinissable ailleurs avec une stricte conservation de la pensée. Il ne s’agit pas cependant que cette pensée joue à faux, qu’elle déraisonne, il s’agit qu’elle se produise, qu’elle jette des feux, même fous. Il s’agit qu’elle existe. Et je prétends, moi, entre autres, que je n’ai pas de pensée.

    Mais ceci fait rire mes amis.

    Et cependant&nbsp;!

    Car je n’appelle pas *avoir de la pensée*, moi, voir juste et je dirai même *penser* juste, avoir de
    la pensée, pour moi, c’est *maintenir* sa pensée, être en état de se la manifester à soi-même et qu’elle puisse répondre à toutes les circonstances du sentiment et de la vie. Mais
    principalement *se répondre à soi*.

    Car ici se place cet indéfinissable et trouble phénomène que je désespère de faire entendre à personne et plus particulièrement à mes amis (ou mieux encore, à mes ennemis, ceux qui me prennent pour l’ombre *que je me sens si bien être*&nbsp;; — et ils ne pensent pas si bien dire, eux, ombres deux fois, à cause d’eux et à cause de moi).

    Mes amis, je ne les ai jamais vus comme moi, la langue pendante, et l’esprit horriblement en arrêt.

    Oui, ma pensée se connaît et elle désespère maintenant de s’atteindre. Elle se connaît, je veux dire qu’elle se soupçonne&nbsp;; et en tout cas elle ne se sent plus. — Je parle de la vie physique, de la vie substantielle de la pensée (et c’est ici d’ailleurs que je rejoins mon sujet), je parle de ce minimum de vie pensante et à l’état brut, — non arrivée jusqu’à la parole, mais capable au besoin d’y arriver, — et sans lequel l’âme ne peut plus vivre, et la vie est comme si elle n’était plus. — Ceux qui se plaignent des insuffisances de la pensée humaine et de leur propre impuissance à se satisfaire de ce qu’ils appellent leur pensée, confondent et mettent sur le même plan erroné des états parfaitement différenciés de la pensée et de la forme, dont le plus bas n’est plus que parole tandis que le plus haut est encore esprit.

    Si j’avais, moi, ce que je sais qui est ma pensée, j’eusse peut-être écrit *l’Ombilic des Limbes*, mais je l’eusse écrit d’une tout autre façon. On me dit que je pense parce que je n’ai pas cessé tout à fait de penser et parce que, malgré tout, mon esprit se maintient à un certain niveau et donne de temps en temps des preuves de son existence, dont on ne veut pas reconnaître qu’elles sont faibles et qu’elles manquent d’intérêt. Mais penser c’est pour moi autre chose que n’être pas tout à fait mort, c’est se rejoindre à tous les instants, c’est ne cesser à aucun moment de se sentir dans son être interne, dans la masse informulée de sa vie, dans la substance de sa réalité, c’est ne pas sentir en soi de trou capital, d’absence vitale, c’est sentir toujours sa pensée égale à sa pensée, quelles que soient par ailleurs les insuffisances de la forme qu’on est capable de lui donner. Mais ma pensée à moi, en même temps qu’elle pèche par faiblesse, pèche aussi par quantité. Je pense toujours à un taux inférieur.
