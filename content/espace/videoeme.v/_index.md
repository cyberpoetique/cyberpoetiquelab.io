---
title: vidéoème.v
---

Une *vidéoème* est une langue qui représente sa décomposition. Elle imagine l’imago de ses métamorphoses fautives. Son hybridation s’infiltre sous rêve comme le tentacule qui se saisit de nos grammaires intestines, elle y fourre la cabale comme on place l’unique balle dans la chambre du revolver. Vingt-quatre fois la brisure de notre icône pour révolution des possibles : sa geste d’argile et d’électricité y retrace, y reflète, y revit. Songe ou songe recommencé : terre rare et bombe au phosphore, nymphose et naine blanche. Son impermanence est notre renaissance : l’art est la contingence de l’image, celle qui se situe au bout du canon.
