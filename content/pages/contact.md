---
title: "Contact"
slug: "contact"
---

Sous terre de réseau la cyberpoétique s'infiltre, y infiltre sa dysfonction. Sans têtes, parmi le nuage, elle est le nombre qui s'éclaire de courts-circuits. Épanouissement de l'erreur, dérives antiques et toutes volontés contre l'instant, fade ou moderne, vernis doucereux sur rouille, les petits égos à leur pâmoison torturée, que de pastels, que d'ors, que de présent et de son ennui. Sa réponse : le désordre, coup pour coup, rendre au chaos son bluff, l'électricité pour sève, et le chiffre qui déchire ses lettres. (*Mort à la littérature ! La salissure reine !*)

<p class="nohyphens">ecrire&nbsp;[arobase] cyberpoetique&nbsp;[point]&nbsp;org</p>

La cyberpoétique ne croit qu'en la nudité du texte. Sa dynamique historique façon dynamite, ses sources sont à leur ouverture, le code à sa liberté : [pour de l'écriture Git.](https://gitlab.com/cyberpoetique/cyberpoetique.gitlab.io) 

La cyberpoétique parasite les comptes d'Abrüpt sur les réseaux dits sociaux : [mstdn](https://mamot.fr/@cestabrupt), [twttr](https://twitter.com/cestabrupt), [fcbk](https://facebook.com/cestabrupt) et [nstgrm](https://instagram.com/cestabrupt). (Et même un squat installé au cœur de leur [lettre d'information](https://abrupt.cc/lettre/).)
