﻿# Cyberpoétique

[La cyberpoétique](https://www.cyberpoetique.org/) ne croit qu'en la nudité du texte. Sa dynamique historique façon dynamite, ses sources sont à leur ouverture, le code à sa liberté : *pour de l'écriture git*. 

Les textes sont mis à disposition selon les termes de la Licence Creative Commons Attribution — Pas d’Utilisation Commerciale — Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0), sauf indication contraire (notamment lorsqu'un texte est placé dans le domaine public volontaire grâce à la licence [CC Zero](LICENSE)). Nous avons néanmoins une lecture adaptative de cette licence (CC BY-NC-SA 4.0), selon les termes de notre [politique de partage](https://abrupt.cc/partage).

Le [code source](https://gitlab.com/cyberpoetique/cyberpoetique.gitlab.io) original de ce site est dédié au domaine public (ceci exclut les librairies libres utilisées pour ce projet). Il est mis à disposition selon les termes de la Licence Creative&nbsp;Commons&nbsp;Zero (CC&nbsp;1.0 Universel) — voir le fichier [LICENSE](LICENSE) à cet effet.

Il est possible de contacter le collectif [La Cyberpoétique](https://www.cyberpoetique.org/contact) par courriel ou sur les différents réseaux sociaux mentionnés sur cette page.

Ce site est une création des camarades du code [Irrealitas](https://irl.st).
