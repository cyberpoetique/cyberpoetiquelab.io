---
title: "{{ substr (replace .Name "-" " ") 7 | humanize }}"
slug: "{{ substr (replace .Name "-" " ") 7 | humanize }}"
date: "{{ now.Format "2006-01-02" }}"
images:
  - "img/{{ substr .Name 7 }}.jpg"
persona:
  - ""
espace:
  - ""
echo:
  - ""
---

