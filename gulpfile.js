// Include Gulp
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    del = require('del'),
    cache = require('gulp-cache');
    gulpif = require('gulp-if');
    imageResize = require('gulp-image-resize');
    changed = require("gulp-changed");
    rename = require("gulp-rename");
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec; // For running a local machine task


// Compress and transform all images
gulp.task('images', function(done) {

  [2000,1200,600,300].forEach(function (size) {
    gulp.src( ['src/img/**/*.{png,gif,jpg}', '!src/img/**/*.mp3', '!src/img/**/*.mp4'])
      .pipe(changed('static/img/'))
      .pipe(imageResize({ width: size }))
      .pipe(cache(imagemin([
        imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
        imagemin.mozjpeg({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
      ], {
          verbose: true
      }
      )))
      .pipe(gulpif(size == 2000, rename(function (path) { path.basename = `${path.basename}`; })))
      .pipe(gulpif(size == 1200, rename(function (path) { path.basename = `${path.basename}-large`; })))
      .pipe(gulpif(size == 600, rename(function (path) { path.basename = `${path.basename}-medium`; })))
      .pipe(gulpif(size == 300, rename(function (path) { path.basename = `${path.basename}-small`; })))
      .pipe(gulp.dest('static/img/'));
    });
  gulp.src( ['src/img/**/*.mp4','src/img/**/*.mp3'])
     .pipe(gulp.dest('static/img/'));
  done();
});

// Clean : clear cache and delete images
gulp.task('clean', function () {
  return del(['static/img/*']);
  cache.clearAll();
});

// Hugo with images compression
gulp.task(
  'hugo-dev',
  gulp.series(
    // 'img',
  function (cb) {
  exec("hugo --cleanDestinationDir --environment development", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

gulp.task(
  'hugo-prod',
  gulp.series(
    // 'img',
  function (cb) {
  exec("hugo --cleanDestinationDir --environment production", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

// Browser-sync
gulp.task('browser-sync-dev', function(cb) {
  browserSync({
    server: {
      baseDir: "public-dev"
    },
    open: false
  }, cb);
});

gulp.task('browser-sync-prod', function(cb) {
  browserSync({
    server: {
      baseDir: "public"
    },
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

gulp.task(
  'img-dev',
  gulp.series('images', 'hugo-dev')
);

gulp.task(
  'img-prod',
  gulp.series('images', 'hugo-prod')
);

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'content/**',
    'assets/**',
    'data/**',
    'layouts/**',
    'static/**',
    'archetypes/**',
    'config/**'
  ], gulp.series('hugo-dev', reload));
  gulp.watch('src/img/**/*', gulp.series('img-dev', reload));
});

gulp.task(
  'default',
  gulp.series('img-dev', 'hugo-dev', 'browser-sync-dev', 'watch')
);

gulp.task(
  'deploy',
  gulp.series('img-prod', 'hugo-prod', 'browser-sync-prod', 'watch')
);
